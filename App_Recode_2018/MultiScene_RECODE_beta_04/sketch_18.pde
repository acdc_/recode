/**
 * Sketch : Maxence Pinchon
 */

class SceneEighteen extends Scene {

      PFont  f18;
      PImage img;
      color C;
      float gris ;
      int x;
      int y;

    void setup() {
      student = "Maxence Pinchon";
      artist = "Nick Barclay";
      title = "Vanishing Type";
      sketchCode = "sketch_18.pde";
      code = loadPde(sketchCode);

            img = loadImage("lovehateb.jpg");
            f18 = createFont("CheltenhamStd-Book.otf", 20);
            textFont(f18);

    }

    void draw() {
      pushMatrix();
      pushStyle();

      translate(screenSizeW/3.2, screenSizeH/8);
      background(0);
      stroke(0);
      strokeWeight(5);
      for ( x=0; x < 600; x+=8) {
        stroke(0, 0, 255);
        strokeWeight(4);
        line(x, 0, x, 600);
      }
      strokeWeight(3);
      for ( x=0; x < 600; x+=8) {
        for ( y=0; y < 600; y+=8) {

          C = img.get(x, y);

          if (C<=color(30)) {
            stroke(255, 0, 0);
            line(x, y-3.5, x, y+3.5);
          }
        }
    }

      popStyle();
      popMatrix();
  }
}