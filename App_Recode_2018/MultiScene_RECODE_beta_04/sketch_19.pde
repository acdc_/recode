/**
 * Sketch : Hugo Molines
 */

class SceneNineteen extends Scene {

    float x, y, taille, rotation, l, s, var;
    color r, v, b;

    void setup() {
      student = "Hugo Molines";
      artist = "Josef Muller Brockmann";
      title = "Musica Viva";
      sketchCode = "sketch_19.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/3.6, screenSizeH/8);
      strokeCap(SQUARE);
      noFill();
      stroke(5);
      background (0);
      r=int(random(0, 250));
      v=int(random(0, 250));
      b=int(random(0, 250));
      strokeWeight(s);
      s=s+1*var;

      if (s==100) {
        var=-1;
      }

      if (s==0) {
        var=1;
      }
      //float angle = 0;
      for (int i = 20; i < 800; i = i+65) {
          float r = random(0, 100);
          josef(300, 300, i, r);
          //angle += r * 0.0013;
        }

        popStyle();
        popMatrix();
      }

      void josef(float x, float y, float taille, float rotation) {

      pushMatrix();
      translate(x, y);
      rotate(radians(rotation));
      //strokeCap(ROUND);
      //stroke(r,v,b);
      stroke(#D8FF00);
      arc(0, 0, taille, taille, radians(0), radians(20));
      //stroke(r,v,b);
      stroke(#FF0318);
      arc(0, 0, taille, taille, radians(20), radians(40));
      //stroke(r,v,b);
      stroke(#0383FF);
      arc(0, 0, taille, taille, radians(40), radians(65));

      //stroke(r,v,b);
      stroke(#D8FF00);
      arc(0, 0, taille, taille, radians(90), radians(110));
      //stroke(r,v,b);
      stroke(#FF0318);
      arc(0, 0, taille, taille, radians(110), radians(135));
      //stroke(r,v,b);
      stroke(#0383FF);
      arc(0, 0, taille, taille, radians(135), radians(155));

      stroke(#D8FF00);
      arc(0, 0, taille, taille, radians(180), radians(200));
      stroke(#FF0318);
      arc(0, 0, taille, taille, radians(200), radians(223));
      stroke(#0383FF);
      arc(0, 0, taille, taille, radians(223), radians(245));

      stroke(#D8FF00);
      arc(0, 0, taille, taille, radians(270), radians(290));
      stroke(#FF0318);
      arc(0, 0, taille, taille, radians(290), radians(315));
      stroke(#0383FF);
      arc(0, 0, taille, taille, radians(315), radians(335));

      popMatrix();

    }
}
