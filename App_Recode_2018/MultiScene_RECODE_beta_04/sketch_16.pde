/**
 * Sketch : Younes Boutarhroucht
 */

class SceneSixteen extends Scene {


    void setup() {
      student = "Younes Boutarhroucht";
      artist = "Kasimir Malevitch";
      title = "Suprematist Compositions";
      sketchCode = "sketch_16.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/4, screenSizeH/12);
      background(#C4C0C0);



      pushMatrix();
      stroke(#1C4FE5);
      strokeWeight(4);
      translate(500,-10);
      rotate(PI/6.0);
      line(158,80,190,580);
      popMatrix();



      //yellow line
      pushMatrix();
      stroke(#FFC012);
      strokeWeight(4);
      translate(557,-5);
      rotate(PI/4.0);
      line(230,170,190,355);
      popMatrix();




      pushMatrix();
      fill(#F51616);
      noStroke();
      translate(375,-370);
      rotate(PI/3.0);
      rect(503,158,65, 15);
      popMatrix();




      pushMatrix();
      noStroke();
      fill(#F2A70F);
      triangle(386,317, 350, 180, 426, 180);
      popMatrix();

      pushMatrix();
      stroke(0);
      strokeWeight(2);
      translate(140,0);
      line(4,175,75,175);
      popMatrix();



      pushMatrix();
      stroke(#FA1717);
      strokeWeight(4);
      translate(120,0);
      line(90,400,140,400);
      popMatrix();

      //red small line
      pushMatrix();
      stroke(#FA1717);
      strokeWeight(4);
      translate(370,195);
      rotate(PI/33.0);
      line(16,380,59,402);
      popMatrix();

      pushMatrix();
      stroke(#FA1717);
      strokeWeight(4);
      translate(165,0);
      line(30,90,70,400);
      popMatrix();

      pushMatrix();
      stroke(#080707);
      strokeWeight(4);
      translate(150,0);
      line(280,210,295,354);
      popMatrix();



      pushMatrix();
      stroke(0);
      strokeWeight(2);
      translate(99,0);
      line(79,218,79,160);
      popMatrix();


      pushMatrix();
      fill(#F51616);
      noStroke();
      translate(375,0);
      rotate(radians(-10));
      rect(11,58,80, 15);
      popMatrix();



      fill(0);
      rectMode(CENTER);
      pushMatrix();
      noStroke();
      fill(#F51919);
      translate(129,0);
      triangle(100,150, 100, 180, 300, 90);
      triangle(100,180,300,80,300,180);
      popMatrix();

      pushMatrix();
      fill(#F51616);
      noStroke();
      translate(90,0);
      rotate(radians(-30));
      rect(85,360,450, 15);
      popMatrix();



      pushMatrix();
      noStroke();
      fill(#050404);
      translate(360,0);
      rotate(radians(-10));
      rect(0,100, 150, 50);
      popMatrix();

      pushMatrix();
      translate(360,0);
      rotate(radians(-10));
      rect(1,60, 50, 50);
      popMatrix();




      pushMatrix();
      fill(#080707);
      strokeWeight(2);
      translate(365,0);
      rotate(radians(-10));
      rect(1,290,60, 14);
      popMatrix();



      //black line
      pushMatrix();
      stroke(0);
      strokeWeight(4);
      translate(650,-1);
      rotate(PI/4.0);
      line(45,400,215,300);
      popMatrix();




      //small quad blue
      pushMatrix();
      fill(#2E487E);
      noStroke();
      translate(350,466);
      rotate(PI/30.0);
      quad(25, 11, 36, 25, 30, 30, 15, 30);
      popMatrix();



      //medium quad blue
      pushMatrix();
      fill(#2E487E);
      noStroke();
      translate(319,570);
      rotate(PI/30.0);
      quad(25, 11, 46, 25, 40, 40, 15, 30);
      popMatrix();

      popStyle();
      popMatrix();
    }
}