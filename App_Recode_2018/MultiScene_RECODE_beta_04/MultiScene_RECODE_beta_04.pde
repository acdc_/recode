/*
 * :::::::::::::::::
 * Multi-Scene Tool
 * :::::::::::::::::
 *
 * Sketch : MultiScene_RECODE_beta_04
 * Git    : https://bitbucket.org/acdc_/recode/
 * Info   : Please read info tab
 */

// NOTES : 08.02.18: TO_DO
// * - Clean up
// *
import processing.video.*;

ArrayList <Scene> theScenes;
Scene currentScene = null;
int currentSceneIndex = 11;

int screenSizeW = 1440; // 1280 || 1440
int screenSizeH = 900; // 760 || 900

boolean showSettings = false;
boolean showCode = false;
boolean autoMode = false;

Timer myTimer;
Timer animeTimer;
Movie movie;


// Global settings for sketch : size
void settings() {
  //size(displayWidth, displayHeight, P3D);
  //println(displayHeight);
  fullScreen(JAVA2D);
  smooth(4);
}

void setup() {
  background(0, 0, 33);
  myTimer = new Timer(25000); // 25 second timer
  
  noCursor();
  movie = new Movie(this, "ich_3.mp4");
  //movie.loop();
  initScenes();
  if(autoMode){
    generateRandomSequence();
    resetAll();
  }else {
    setupScenes();
  }
}


void draw() {
  background(0, 0, 33);
  if (currentScene !=null) {

    if(currentSceneIndex == 21){
      movie.play();
    }else {
      movie.stop();
    }

    currentScene.draw();
    currentScene.showInfo();
}

  ///////////////////////////// > sequenced / key actions
  if (myTimer.sequence(3000, 5000)) {
    currentScene.showCode();
  }

  if(myTimer.sequence(5000, 12000)) {
    currentScene.anime = true;
  }else {
    currentScene.anime = false;
  }

  if(showSettings){
    currentScene.showSettings();
  }
  if(showCode){
    currentScene.showCode();
  }

  if ((myTimer.finished())&&(autoMode)) {
   resetAll();
 }
}


// add each sketch to the list
void initScenes() {
  //theScenes.clear();
  theScenes = new ArrayList<Scene>();
  theScenes.add( new SceneOne() );
  theScenes.add( new SceneTwo() );
  theScenes.add( new SceneThree() );
  theScenes.add( new SceneFour() );
  theScenes.add( new SceneFive() );
  theScenes.add( new SceneSix() );
  theScenes.add( new SceneSeven() );
  theScenes.add( new SceneEight() );
  theScenes.add( new SceneNine() );
  theScenes.add( new SceneTen() );
  theScenes.add( new SceneEleven() );
  theScenes.add( new SceneTwelve() );
  theScenes.add( new SceneThirteen() );
  theScenes.add( new SceneFourteen() );
  theScenes.add( new SceneFifteen() );
  theScenes.add( new SceneSixteen() );
  theScenes.add( new SceneSeventeen() );
  theScenes.add( new SceneEighteen() );
  theScenes.add( new SceneNineteen() );
  theScenes.add( new SceneTwenty() );
  theScenes.add( new SceneTwentyOne() );
  theScenes.add( new SceneTwentyTwo() );
}


void movieEvent(Movie movie) {
  movie.read();
}
