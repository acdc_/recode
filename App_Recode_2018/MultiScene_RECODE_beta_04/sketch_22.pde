/**
 * Sketch : Meddy
 */

class SceneTwentyTwo extends Scene {
      PImage type1, type2, type3, type4, type5, type6;
      String signe = "X";
      color rouge;
      color clr;
      int x, y, x1, y1;
      PImage img [] ;

    void setup() {
      student = "Meddy";
      artist = "Ken Knowlton";
      title = "Studies In Perception #1";
      sketchCode = "sketch_22.pde";
      code = loadPde(sketchCode);

      img = new PImage [14];
      for (int i =1; i < 14; i ++) {
        img[i] = loadImage( i +".png");
        // img[i].resize(7, 0);
      }
    }

    void draw() {
      pushMatrix();
      pushStyle();
      background(0);
      translate(90, 50);
      //imageMode(CENTER);
      drawMovie();

      popStyle();
      popMatrix();
    }

    void drawMovie() {
          movie.loadPixels();
          for (int x=1; x<=screenSizeW; x+=7) {
            for (int y=1; y<=screenSizeH; y+=7) {

        clr=movie.get(x, y);
        fill(clr);

        float value = brightness(clr);  // Sets 'value' to 255

        fill(value);

        int index = int(map(value, 0, 255, 14, 1));


        if (index == 1) {
          pushMatrix();
          translate(x, y);
          image(img[1], 0, 0);
          popMatrix();
        }

        if (index == 2) {
          pushMatrix();
          translate(0, 0);
          image(img[2], x, y);
          popMatrix();
        }

        if (index == 3) {
          pushMatrix();
          translate(0, 0);
          image(img[3], x, y);
          popMatrix();
        }

        if (index == 4) {
          pushMatrix();
          translate(0, 0);
          image(img[4], x, y);
          popMatrix();
        }

        if (index == 5) {
          pushMatrix();
          translate(0, 0);
          image(img[5], x, y);
          popMatrix();
        }
        if (index == 6) {
          pushMatrix();
          image(img[6], x, y);

          popMatrix();
        }
        if (index == 7) {
          pushMatrix();
          translate(0, 0);
          image(img[7], x, y);
          popMatrix();
        }
        if (index == 8) {
          pushMatrix();
          translate(0, 0);
          image(img[8], x, y);
          popMatrix();
        }
        if (index == 9) {
          pushMatrix();
          translate(0, 0);
          image(img[9], x, y);
          popMatrix();
        }
        if (index == 10) {
          pushMatrix();
          translate(0, 0);
          image(img[10], x, y);
          popMatrix();
        }
        if (index == 11) {
          pushMatrix();
          translate(0, 0);
          image(img[11], x, y);
          popMatrix();
        }
        if (index == 12) {
          pushMatrix();
          translate(0, 0);
          image(img[12], x, y);
          popMatrix();
        }
        if (index == 13) {
          pushMatrix();
          translate(0, 0);
          image(img[13], x, y);
          popMatrix();
        }
      }
  }
  }
}