/**
 * Sketch : Robin Ansart
 */

 class SceneOne extends Scene {

   float x, y, a, b, e, f, h, k, l, m, n, o;
   color r, v, g;
   int SEED;
   float taille = 30;
   int timer = 0;


 void setup () {
   a=0;
   b=0;
   background(0);
   SEED = (int)random(1000);
   student = "Robin Ansart";
   artist = "Vera Molnar";
   title = "Dialog Between Emotion And Method";
   sketchCode = "sketch_01.pde";
   code = loadPde(sketchCode);
 }


 void draw() {
    rectMode(CENTER);
    noFill();
     pushMatrix();
     translate(sceneWidth/3.2, sceneHeight/10);
     scale(0.85);
     background(0,0,33);
     fill(0, 7);
     noStroke();
     rect(0,0,sceneWidth, sceneHeight);
     strokeWeight(1.7); //épaisseur du trait
     randomSeed(SEED);

     for (int j =0; j<36; j=j+1) { //boucle pour les 36 carrés
       x=20; //valeur du x à la base
       y=120; //valeur du y à la base

       r=int(random(3, 255)); // int est là pour arrondir parce que impossible d'avoir une valeur décimale
       v=int(random(3, 233));
       g=int(random(3, 255));


       stroke(r, v, g); // couleur du contour


       pushMatrix(); //début du calque
       translate(a, b);

       for ( int i = 0; i<10; i=i+1 ) { //lancement de la boucle, tourne 10x
         x=x+4; //le carré du milieu sera 4 px + à droite
         y=y-4; //le carré du milieu sera 4 px + bas
         quad (x+e, x+f, x+h, y+k, y+l, y+m, y+n, x+o); //crée les points du carré


         e=random(-taille, taille);
         f=random(-taille, taille);
         h=random(-taille, taille);
         k=random(-taille, taille);
         l=random(-taille, taille);
         m=random(-taille, taille);
         n=random(-taille, taille);
         o=random(-taille, taille);

         //println(e);
       }
       popMatrix(); //fin du calque


       a=a+120;

       if (a>600) //retour à la ligne des carrés
       {
         b=b+120;
         a= 0;
       }

       if (b > 600 ) {
         b=0;
       }
     }
     timer++;
     if(timer>=100){
      timer = 0;
       SEED = (int)random(1000);
     }
     popMatrix();
   }
 }