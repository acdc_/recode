/**
 * Sketch : Evan Lelièvre
 */

class SceneTwelve extends Scene {


  float[] x = new float[11];
  float[] y = new float[11];
  float segLength = 45;

    void setup() {
      student = "Evan Lelièvre";
      artist = "Julio Le Parc";
      title = "Modulation 1159";
      sketchCode = "sketch_12.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/2, screenSizeH/2);
      fill(255);
      noStroke();
      background(0);

      float rayon = sin(frameCount * 0.0135) * width/2.8;
      float dir = map(rayon, -width/2.8, width/2.8, 1, -1);
      float xPos = cos(frameCount*0.05) * rayon;
      float yPos = (sin(frameCount*0.05) * rayon) * dir;
       float n = noise(xPos * 0.0035, yPos * 0.0035, frameCount*0.015) * 33;

       dragSegment(0, xPos+n, yPos+n);
        for(int i=0; i<x.length-1; i++) {
          dragSegment(i+1, x[i], y[i]);
        }

      popStyle();
      popMatrix();
    }

    void dragSegment(int i, float xin, float yin) {
        float dx = xin - x[i];
        float dy = yin - y[i];
        float angle = atan2(dy, dx);
        x[i] = xin - cos(angle) * segLength;
        y[i] = yin - sin(angle) * segLength;
        segment(x[i], y[i], angle);
      }

      void segment(float x, float y, float a) {
        pushMatrix();
        translate(x, y); noStroke();
        fill(255,120);
        rotate(a);
        rect(0, 0, 120, 120);
        popMatrix();
      }
}
