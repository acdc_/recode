/**
 * Sketch : Clézio Descluse
 */

class SceneEleven extends Scene {

  PFont font;
  float x, y;
  char[] chars ={'█', '█', '▓', '▒', '░', '#', '≡', '%', '$', '@', '&'};
  char c;
  int index;
  int marge = 30;
  int seed;

  void setup() {
    student = "Clézio Descluse";
    artist = "Jodi";
    title = "New York Boogie";
    sketchCode = "sketch_11.pde";
    code = loadPde(sketchCode);
    font = createFont("font.ttf", 50);
    seed = (int)random(1000);
  }

  void draw() {
    background(0);
    pushStyle();
    rectMode(CENTER);
    textFont(font);
    textAlign(CENTER, CENTER);
    randomSeed(seed);
    pushMatrix();
    translate(sceneWidth/7, sceneHeight/12);
    textFont(font);
    scale(0.75);
    for (int y=marge; y<sceneHeight-marge; y+=30) {
      for (int x=marge; x<sceneWidth-70; x+=30) {
        
        int dice = (int)random(8);
        if (dice<2) {
          index = (int)random(33, 127);
          c = char(index);
          fill(255);
          text(c, x, y);
        } else if (dice>2 && dice<4) {
          color theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme1(x, y, longeur, theColour);
        } else if (dice>4 && dice<6) {
          color theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme2(x, y, longeur, theColour);
        } else if (dice>6 && dice<8) {
          color theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme3(x, y, longeur, theColour);
          x=x-15;
        } else {
        }
      }
    }
    popMatrix();
    popStyle();
  }

  void keyPressed() {
    seed = (int)random(0, 1000);
  }

  void forme1(float _x, float _y, float _len, color _c) {
    noStroke();
    pushMatrix();
    translate(_x, _y);
    pushMatrix();
    translate(5, 6);
    rect(0, 0, _len, 30);
    popMatrix();
    popMatrix();
  }

  void forme2(float _x, float _y, float _len, color _c) {
    int dice = (int)random(6);
    if (dice<3) {
      color theColour = color(random(100, 255), random(5, 255), random(0, 255));
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, _len, 30);
      popMatrix();
      popMatrix();
    }
  }



  void forme3(float _x, float _y, float _len, color _c) {
    int dice = (int)random(6);
    if (dice<3) {
      color theColour = color(0);
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, 10, 10);
      popMatrix();
      popMatrix();
    } else {
      color theColour = color(255);
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, 10, 10);
      popMatrix();
      popMatrix();
    }
  }
}