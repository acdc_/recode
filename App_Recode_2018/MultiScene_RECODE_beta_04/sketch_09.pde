/**
 * Sketch : Rose-Marie Devignes
 */

class SceneNine extends Scene {

  color couleur [] = {color(62, 128, 208), color(222, 255, 0), color (249, 141, 157), color(87, 225, 77)};

  void setup() {
    student = "Rose-Marie Devignes";
    artist = "Vera Molnar";
    title = "Un, Deux, Trois";
    sketchCode = "sketch_09.pde";
    code = loadPde(sketchCode);
  }

  void draw() {
    rectMode(CORNER);
    pushMatrix();
    pushStyle();
    translate(sceneWidth/3.6, 100);
    scale(0.85);
     // ADD the seed somewhere > animations !
     if(anime){

     }else {
       randomSeed(1);
     }
    background(233);
      fill(0);
      stroke(0);
      strokeWeight(2);
      rectMode(CENTER);

      for (int y=50; y<sceneHeight-720; y+=40) {
        for (int x=0; x<800; x+=40) {
          float angleRandom=random(-75, 75);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          if (anime) {
              fill(couleur [ int(random(0, 4))]);
          }else {
            fill(0);
          }
         // rect(0, 10, 35, 8, 200);
          rect(0, -10, 40, 10, 200);
          popMatrix();
      }
    }

      for (int y=sceneHeight-690; y<sceneHeight-440; y+=40) {
        for (int x=0; x<800; x+=40) {
          float angleRandom=random(-75, 75);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          rectMode(CENTER);
          rect(0, -10, 40, 10, 200);
          rect(5,20, 40, 10, 200);
          popMatrix();
        }
      }

      for (int y=sceneHeight-420; y<sceneHeight-100; y+=40) {
        for (int x=0; x<800; x+=40) {
          float angleRandom=random(-180, 180);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          rectMode(CENTER);
          rect(0, -10, 40, 10, 200);
          rect(5, 5, 40, 10, 200);
          rect(10, 20, 40, 10, 200);
          popMatrix();
        }
      }
    popStyle();
    popMatrix();
    }
}
