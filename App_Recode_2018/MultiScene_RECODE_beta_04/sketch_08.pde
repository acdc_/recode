/**
 * Sketch : Rose-Marie Devignes
 */

class SceneEight extends Scene {

    color couleur [] = {color(62,128,208), color(222,255,0), color (249,141,157), color(87,225,77)};

    void setup() {
      student = "Rose-Marie Devignes";
      artist = "John Armleder";
      title = "Cast Iron";
      sketchCode = "sketch_08.pde";
      code = loadPde(sketchCode);

    }

    void draw() {

      pushMatrix();
      pushStyle();
      rectMode(CORNER);
      background(0);
      strokeCap(SQUARE);
      randomSeed(1);
      //translate(sceneWidth/7, sceneHeight/8);
      //scale(0.7);
      background(#E3DCCA);
      for ( int y=5; y<sceneHeight; y=y+90) {
        for (int x=10; x<(sceneWidth/3);x=x+91) {
          noStroke();
          fill(couleur [ int(random(0, 4))]);
          ellipse(x+50, y+70, sceneWidth/35, sceneWidth/35);
        }
      }
      for ( int x=-10; x<(sceneWidth/2.25); x=x+70) {
        for (int y=0; y<(sceneHeight/2.8); y=y+10) {
          stroke(255, 0, 0);
          strokeWeight(sceneWidth/40);
          line(x+(sceneWidth/3), y, x+(sceneWidth/3), sceneHeight);
        }
      }

      popStyle();
      popMatrix();
    }
}
