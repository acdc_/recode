/**
 * Sketch : Emiline Herveau
 */

class SceneSeven extends Scene {
    
    float epaisseur = 0.1;
    float phi = (sqrt(5)+1)/2;
    int SEED;
    int timer;
  
    void setup() {
      student = "Emiline Herveau";
      artist = "Piet Mondrian";
      title = "Composition II en rouge, bleu, jaune";
      sketchCode = "sketch_07.pde";
      code = loadPde(sketchCode);
      background(255);
      SEED = (int)random(1000);
      timer = 0;

    }

    void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      translate(sceneWidth/8, sceneHeight/12); 
      randomSeed(SEED);
              stroke(0);
        strokeWeight(0.05);
      translate(sceneHeight/phi, 0);
      scale(sceneHeight*0.75);
      fill(0);
      rect(-50, -50, 600, 600);
      for (float i=0; i<12; i++) {
        int poss = 0;
        random(poss);

        int hasard = int(random(0, 4));

        color couleur [] = {color(#FF0329), color(#1956F7), color(#FEFF00), color(#FFFFFF)};
        fill(couleur[hasard]);



        rect(0, 0, 1, 1);
        epaisseur = epaisseur  - 0.005;
        scale(1/phi);
        rotate(PI/2);
        translate(1/phi, 0);
      }
      timer++;
      if (timer>=60) {
        timer = 0;
        SEED = (int)random(1000);
      }
      popStyle();
      popMatrix();
    }
}