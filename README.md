# RECODE #

RECODE ou comment percevoir, puis interpréter une œuvre abstraite, géométrique, minimaliste, constructiviste, ...avec le médium code informatique. 
Vous analyserez une œuvre qui a permis de penser l'art algorithmique. Vous en dégagerez les règles de constructions permettant de reproduire de manière systématique cette image.

![vera_01.jpg](https://bitbucket.org/repo/LoaEMaM/images/102707088-vera_01.jpg)

### Contents ###

* Outils
* Sketches dev

### Références ###
* [Ted Davis - Processing Nees](http://www.dusie.ch/processingnees/)


### Who do I talk to? ###

* [Mark Webster](https://bitbucket.org/mwebster_/)
* [Keyvane Alinaghi](https://bitbucket.org/keyvane/)
* http://www.esac-cambrai.net/
* Version : 0.1