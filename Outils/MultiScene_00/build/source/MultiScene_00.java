import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class MultiScene_00 extends PApplet {

/*
 * :::::::::::::::::
 * Multi-Scene Tool
 * :::::::::::::::::
 *
 * Sketch : MultiScene_00
 * Git    : https://bitbucket.org/acdc_/recode/
 * Note : Read Info tab
 *
 */

// NOTES : add date : TO_DO
// *
// *

ArrayList <Scene> theScenes;
Scene currentScene = null;
int currentSceneIndex = 0;
int screenSizeW = 1280;
int screenSizeH = 760;

boolean showSettings = false;

// Global settings for sketch : size
public void settings() {
  size(screenSizeW, screenSizeH, P3D);
  smooth(4);
}

public void setup() {
  background(0, 0, 33);
  noCursor();
  addScenes();
  setupScenes();
}


public void draw() {
  background(0, 0, 33);

  currentScene.draw();
  currentScene.showInfo();


  ///////////////////////////// > sequenced / key actions
  if(showSettings){
    currentScene.showSettings();
  }
}


public void addScenes(){
  theScenes = new ArrayList<Scene>();
  // add each sketch to the list
  theScenes.add( new SceneOne() );
  theScenes.add( new SceneTwo() );
  theScenes.add( new SceneThree() );
}
/*
 * Abstract Scene Class
 *
 */


abstract class Scene {

  String student = " ";
  String artist = " ";
  String title = " ";
  String sketchCode, code;
  int sceneWidth;
  int sceneHeight;
  PFont infoFont, systemFont, codeFont, titleFont;

  public Scene(){
    infoFont = createFont("Iosevka-Heavy", 30);
    systemFont = createFont("Iosevka-light", 15);
    codeFont = createFont("Iosevka-light", 12);
    titleFont = createFont("Iosevka-medium-oblique", 24);
  }


  public abstract void setup();
  public abstract void draw();
  //abstract void mousePressed();
  //abstract void keyPressed();

  /*
    * Set scene dimensions depending on gloable screen size settings
   */
  public void setSceneDimensions() {
    this.sceneWidth = screenSizeW;
    this.sceneHeight = screenSizeH;
  }

  public int getSceneWidth() {
    return sceneWidth;
  }

  public int getSceneHeight() {
    return sceneHeight;
  }


  /*
   * Display sketch settings
   */
  public void showSettings() {
    pushStyle();
    String out = "";
    textFont(systemFont, 15);
    textSize(15);
    textAlign(LEFT);
    out += "--------------------------\n";
    out += "fps: " + nf(frameRate, 0, 1) + "\n";
    //out += "current animation: " + author + "\n";
    out += "current animation: " + currentSceneIndex + "\n";//
    pushMatrix();
    translate(45, 20);
    fill(0, 200, 255);
    text(out, 0, 0);
    popMatrix();
    popStyle();
  }

  /*
   * Display info for current animation
   *
   */
  public void showInfo() {
    pushStyle();
    textFont(infoFont, 26);
    textSize(30);
    //textLeading(18);
    pushMatrix();
    translate(45, height - 90);
    fill(200, 255, 0);
    text(artist, 0, 0);
    popStyle();

    pushStyle();
    textFont(titleFont, 20);
    textSize(24);
    fill(200, 255, 0);
    text(title, 0, 28);
    popStyle();

    pushStyle();
    textFont(systemFont, 14);
    textSize(16);
    fill(0, 200, 255);
    text("cod\u00e9 par "+student, 0, 60);
    popStyle();
    popMatrix();

  }

}
/**
 * Various functions
 */



/**
 * Init scenes : size & other setup configs
 */

 public void setupScenes(){
   currentScene = theScenes.get(currentSceneIndex);
   println("Current scene chosen : "+currentSceneIndex);
   for(Scene s : theScenes){
     s.setSceneDimensions();
     s.setup();
   }
 }

 public void clearScene(){
   background(0,0,33);
 }

 public void keyPressed(){

   if(keyCode == RIGHT){
     // get next scene
     if (currentSceneIndex <theScenes.size()-1) {
       currentSceneIndex++;
       currentScene = theScenes.get( theScenes.indexOf(currentScene)+1 );
     }
   }

   if(keyCode == LEFT){
     // get prev scene
     if (currentSceneIndex > 0) {
       currentSceneIndex--;
       currentScene = theScenes.get(
         theScenes.indexOf(currentScene)-1 );
     }
   }

   if(key == 'r'){
     // get random scene
   }

   if(key == 'i'){
    showSettings = !showSettings;
   }
 }
/*
 ------------------------
 PROJECT : PERSONAL/TEACHING
 02.02.2018

 -------------------------
 Sketch : MultiScene_RECODE_00
 Sketch Version : 0.1
 Langauge : Processing
 Version : 3.3

 * Summary : Simple tool for presenting multiple Processing
 *           sketches all in one program.
 *
 * Author :  MWebster 2018 : www.fabprojects.codes
 * Code :    https://bitbucket.org/acdc_/recode/

 -------------------------
 LIBRARIES
 - www.processing.org

 -------------------------
 USE
 1) Create a new file/new tab and paste your full sketch.
 2) Enclose your sketch within a class named, Scene+number.
     Eg. SceneOne, SceneTwo...
 3) Make sure you extend this class with "...extends Scene"
 4) Remove size() function in setup() and add :
      - student = "yourName";
      - artist
      - title
      - sketchCode = "file name given to tab" >>> eg. sketchCode = "sketch_02.pde";

 5) See examples in tabs & a basic setup config in model tab:
    sketch_01.pde / sketch_02.pde / sketch_03.pde


 -------------------------
 * NOTES
 *

  -------------------------
    LICENSE

    MULTI-SCENE_01
    Copyright (C) 2018 Mark Webster
    www.fabprojects.codes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 // END OF INFO

 */
/**
 * Sketch : MW17
 *
 */

class SceneOne extends Scene {


  int step;
  float theta;
  int num = 20;


  public void setup() {
    background(0);
    noFill();
    student = "MW";
    title = "Pull";
    sketchCode = "sketch_01.pde";
     step=sceneHeight/num;
  }

  public void draw() {
    background(3,3, 43);
    stroke(255, 255, 0);


    for (int i=0; i<sceneHeight; i+=step) {
      float strokeW = map(sin(theta+(num*i)), -1, 1, 1.5f, 48);
      strokeWeight( strokeW );
      line(0, i, width, i);
    }

    theta += .05f;
  }
}
/**
 * Sketch : MW
 *
 */

class SceneTwo extends Scene {

  public void setup() {
    student = "MW";
    title = "3D Fields";
    sketchCode = "sketch_02.pde";
}

/////////////////////////// DRAW ////////////////////////////

public void draw() {
  background(0);
  int numX = 120;
  int numY = 80;
  int pnts = 4;

  //Center our grid
  float offpntsX = -numX * pnts / 2;
  float offpntsY = -numY * pnts / 2;
  pushMatrix();
  translate(sceneWidth/2, (numY+sceneHeight/2.3f));

  float snx = sin(frameCount * 0.0007f) * 6;
  float sny = sin(frameCount * 0.0003f) * 6;


  stroke(255);
  strokeWeight(1.7f);
  for (int j=0; j<numY-1; j++) {
    for (int i=0; i<numX-1; i++) {

      //Calculate our x & y coordinates for our points
      float px = offpntsX + i * pnts;
      float py = offpntsY + j * pnts;

      float pz = noise(px * 0.004f + snx, py * 0.004f + sny) * 300;
      point(px, py, pz);

    }
  }
  popMatrix();
}

}
/**
 * Sketch : Arnaud Chemin
 */


class SceneThree extends Scene {
  float speed=0.005f;

  public void setup() {
    student = "Arnaud Chemin";
    title = "Duchamp";
    sketchCode = "sketch_03.pde";
  }

  public void draw() {
  background(0);
  int a = 0;
  for (int i=800; i>0; i-=30) {
      a++;
      noStroke();

      pushMatrix();
      translate(sceneWidth/2, sceneHeight/2);
      rotate(speed/15);
      if (a % 2 == 0) {
        fill(220, 30, 68); //couleur 1
      } else {
        fill(22, 80, 220); //couleur 2
      }
      ellipse((i/2.5f)-100,0, i, i);
      popMatrix();

      speed += 0.035f;
    }
}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "MultiScene_00" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
