/**
 * Sketch : Hans Kuiper
 *
 */

class SceneTen extends Scene {

  float x1;
  float y1;
  float x2;
  float y2;
  float angleStep;

  void setup() {
    author = "Emiline";
    sketchName = "Hans Kuiper";
    sceneWidth = 800;
    sceneHeight = 800;
  }
    void draw(){
      background(240);
      fill(0);
      rectMode(CENTER);
      pushMatrix();
      translate(150, 165);
      scale(1.3);
      float angle = 0;

      for (int y= 2; y < 410; y = y + 40) {
        for (int x=2; x< 410; x = x + 40) {
          //float m = map(mouseX, 0, width, 0.001, 0.1);
          //float angle = noise(x * m, y * m) * 3;

          pushMatrix(); // matrix servent à créer des calques
          translate(x, y); // bouger mes nouveaux calques
          rotate(radians(angle + angleStep)); // rotation de x degres

          rect(0, 0, 35, 12);
          rect(0, 0, 12, 35);
          popMatrix();
          //deg = deg + 9;
          angle+=9;
        }

      }
      popMatrix();
      if(anime){
        angleStep += 0.9;
      }
  }

}
