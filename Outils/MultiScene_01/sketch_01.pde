/**
 * Sketch : MW17
 *
 */

class SceneOne extends Scene {

  void setup() {
    author = "Clémence";
    sketchName = " ????? ";
    sceneWidth = 800;
    sceneHeight = 800;
    noStroke();
  }

  void draw() {
    pushStyle();
    rectMode(CORNER);
    background(0,0,33);

    pushMatrix();
    translate(150,150);
    fill(255,0,0);
    rect(-20, -20, 580,600);

    fill(112, 34, 142);
    rect(423, 60, 82, 80);

    fill(125, 23, 25);
    rect(363, 200, 101, 101);

    fill(255, 67, 151);
    rect(363, 300, 121, 118);

    fill(255, 67, 151);
    rect(235, 0, 150, 155);

    fill(125, 23, 25);
    rect(0, 80, 115, 137);

    fill(125, 23, 25);
    rect(75, 0, 40, 85);

    fill(125, 23, 25);
    rect(75, 0, 310, 20);

    fill(125, 23, 25);
    rect(75, 20, 160, 20);

    fill(112, 34, 142);
    rect(23, 120, 90, 97);

    fill(255, 67, 151);
    rect(0, 340, 90, 198);

    fill(255, 67, 151);
    rect(90, 400, 82, 158);

    fill(255, 67, 151);
    rect(117, 480, 245, 65);

    fill(125, 23, 25);
    rect(153, 400, 110, 118);



    // rectangle rouge milieu en bas
    fill(255, 0, 0);
    rect(15, 417, 100, 100);

    fill(67, 85, 20);
    maForme(0, 0, 550, 22);

    // bleu et rose
    fill(255, 67, 151);
    rect(255, 20, 59, 60);

    pushMatrix();
    fill(131, 208, 245);
    rotate(radians(90));
    translate(-235, -332);
    maForme(255, 23, 55, -30);
    popMatrix();


    // bleu f et rouge et bordeau

    fill(255, 0, 0);
    rect(75, 55, 160, 162);

    //b
    fill(125, 23, 25);
    rect(75, 55, 40, 65);

    //v
    fill(112, 34, 142);
    rect(75, 120, 40, 97);

    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(-191, -255);
    maForme(255, 23, 155, 30);
    popMatrix();

    // Bleu moyen
    //rouge
    fill(255, 0, 0);
    rect(253, 150, 133, 110);
    //b
    noStroke();
    fill(125, 23, 25);
    rect(365, 200, 40, 60);

    //rose


    fill(255, 67, 151);
    rect(253, 99, 133, 57);

    //rouge petit
    fill(255, 0, 0);
    rect(385, 95, 20, 105);

    fill(9, 98, 198);
    maForme(255, 100, 150, -15);
    fill(125, 23, 25);
    rect(365, 280, 98, 20);
    //rouge 1


    fill(255, 0, 0);
    rect(245, 275, 120, 125);


    fill(255, 0, 0);
    rect(243, 380, 122, 100);
    fill(255, 67, 151);
    rect(365, 300, 100, 118);

    fill(125, 23, 25);
    rect(243, 400, 20, 80);

    fill(255, 0, 0);
    rect(365, 420, 100, 60);


    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(220, 20);
    maForme2(55, -482, 205, -15);
    popMatrix();



    // bleu moyen
    //rose

    fill(255, 67, 151);
    rect(135, 435, 20, 80);

    fill(125, 23, 25);
    rect(153, 435, 60, 80);

    pushMatrix();
    fill(9, 98, 198);
    rotate(radians(90));
    translate(-19, -25);
    maForme(463, -187, 75, 30);
    popMatrix();


    //le dernier
    //rouge 1
    fill(255, 0, 0);
    rect(34, 235, 180, 105);

    //rouge 2


    fill(255, 0, 0);
    rect(94, 340, 120, 60);

    //rose
    fill(255, 67, 151);
    rect(34, 340, 80, 76);
    //rose 2

    fill(255, 67, 151);
    rect(114, 400, 40, 20);

    fill(125, 23, 25);
    rect(154, 400, 60, 20);
      pushMatrix();
      fill(101, 200, 245);
      maForme(35, 240, 180, -15);
      popMatrix();
      popMatrix();
      popStyle();
  }


  void maForme(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    noStroke();
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }

  void maForme2(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }
}
