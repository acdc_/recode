/**
 * Sketch : Sophie Mialon
 */

 class SceneFour extends Scene {

   void setup() {
     author = "Sophie Mialon";
     sketchName = "Bridget Riley";
     sceneWidth = 800;
     sceneHeight = 800;
   }

   void draw() {
     background(200);
     rectMode(CORNER);
     randomSeed(1);
     pushMatrix();
     translate(20, 125);
     drawQuad(500, 700);
     makeShape(760, 530);
     popMatrix();
   }
   void makeShape(int w, int h) {
     fill(230);
     noStroke();
     rect(0, 0, w, 40);
     rect(0, 0, 50, h);
     rect(710, 0, 50, h);
     rect(0, 490, w, 40);
   }

   void drawQuad(int docHeight, int docWidth) {
     color[] colorList = {color(237, 216, 189), color(18, 152, 196), color(237, 148, 32), color(85, 129, 57), color(232, 95, 81), color(129, 150, 203), color(246, 231, 212), color(2, 2, 2)};
     for (int y = 40; y < docHeight+20; y+=20) {
       for (int x = 38; x < docWidth; x+=38) {
         //rect(x, y, 40, 25);
         color randomColor = colorList[(int)random(colorList.length) ];
         noStroke();
         fill(randomColor);
         quad(x, y, x+38, y-20, x+38, y, x, y+24);
       }
     }
   }
 }
