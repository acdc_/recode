/**
 * Sketch : Robin
 */

 class SceneSix extends Scene {


   void setup(){
     author = "Hugon_Yseult";
     sketchName = "Grant Wiggins";
   }

   void draw(){
     rectMode(CORNER);
     pushMatrix();
     translate(130,140);
     background(#708186);
     fill(255);
     noStroke();
     rect(0, 258.5, 509, 258.5);


   for (int i = 210; i < 300; i+=10) {
     fill(#FF368D);
     rect(125, i, 385, 5);
   }

   for (int i = 215; i < 295; i+=10) {
     fill(#6FA3D8);
     rect(125, i, 385, 5);
   }



   for (int i = 230; i<400; i+=92) {
     for (int j = 75; j<400; j+=92) {

      pushMatrix();
       translate(j, i);
       for (int k = 0; k < 48; k+=1) {
         fill(255,0,0);
         rect(k, k, 46, 46);
         if (k == 47) {
           fill( #FF00E2);
           rect(0, 0, 46, 46);
         }
       }
      popMatrix();



     }
   }

   for (int i = 276 ; i<400; i+=92) {
     for (int j = 122; j<400; j+=92) {

       pushMatrix();
       translate(j, i);
       for (int k = 0; k < 48; k+=1) {
         fill(255,0,0);
         rect(k, k, 46, 46);
         if (k == 47 ) {
           fill( #FF00E2);
           rect(0, 0, 46, 46);

         }
       }
       popMatrix();

     }
   }

   for (int y = 168 ; y<400 ; y+=92){
     fill(#FF00E2);
     rect(y,322,46,46);
   }

   for (int y = 122 ; y<400 ; y+=92){
     fill(255,0,0);
     rect(y,322,46,46);
   }

   for (int y = 0; y < 200; y = y + 40) {
    for (int i = 125 ; i < 200 ; i= i +40){
       fill(#FF368D);
       rect(i, 10+y, 385, 5 );
       fill(#6FA3D8);
       rect(i, 15+y, 385, 5 );
       fill(#FF368D);
       rect(60+i, 20+y, 385, 5 );
       fill(#6FA3D8);
       rect(60+i, 25+y, 385, 5 );
       fill(#FF368D);
       rect(120+i, 30+y, 385, 5 );
       fill(#6FA3D8);
       rect(120+i, 35+y, 385, 5 );
       fill(#FF368D);
       rect(180+i, 40+y, 385, 5 );
       fill(#6FA3D8);
       rect(180+i, 45+y, 385, 5 );
     }
    }
    fill(#708186);
    rect(509, 0, 300, 400);
    popMatrix();
   }
 }
