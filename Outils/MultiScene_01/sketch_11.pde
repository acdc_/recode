/**
 * Sketch : JODI
 *
 */

class SceneEleven extends Scene {


    PFont font;
    float x, y;
    char[] chars ={'█', '█', '▓', '▒', '░', '#', '≡', '%', '$', '@', '&'};
    char c;
    int index;
    int marge = 60;
    int seed;

    void setup() {
      author = "CLézio Descluse";
      sketchName = "Jodi";
      sceneWidth = 800;
      sceneHeight = 800;
      font = createFont("font.ttf", 50);
      seed = (int)random(1000);
    }


    void draw() {
      background(0);
      pushStyle();
      rectMode(CENTER);
      textFont(font);
      textAlign(CENTER, CENTER);
      randomSeed(seed);
      pushMatrix();
      translate(0, 100);
      scale(0.7);
      for (int y=marge; y<height-marge; y+=30) {
        for (int x=marge; x<width-marge/2; x+=30) {
          textFont(font);
          int dice = (int)random(8);
          if (dice<2) {
            index = (int)random(33, 127);
            c = char(index);
            fill(255);
            text(c, x, y);
          } else if (dice>2 && dice<4) {
            color theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme1(x, y, longeur, theColour);
          } else if (dice>4 && dice<6) {
            color theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme2(x, y, longeur, theColour);
          } else if (dice>6 && dice<8) {
            color theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme3(x, y, longeur, theColour);
            x=x-15;
          } else {
          }
        }
      }
      popMatrix();
      popStyle();
    }

    void keyPressed() {
        seed = (int)random(0, 1000);
    }

    void forme1(float _x, float _y, float _len, color _c) {
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, _len, 30);
      popMatrix();
      popMatrix();
    }

    void forme2(float _x, float _y, float _len, color _c) {
      int dice = (int)random(6);
      if (dice<3) {
        color theColour = color(random(100, 255), random(5, 255), random(0, 255));
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, _len, 30);
        popMatrix();
        popMatrix();
      }
    }



    void forme3(float _x, float _y, float _len, color _c) {
      int dice = (int)random(6);
      if (dice<3) {
        color theColour = color(0);
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, 10, 10);
        popMatrix();
        popMatrix();
      } else {
        color theColour = color(255);
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, 10, 10);
        popMatrix();
        popMatrix();
      }
}
}
