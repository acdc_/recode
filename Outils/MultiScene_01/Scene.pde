/*
 * Scene class manager
 *
 */


class Scene {

  String author = "Anonymous";
  String sketchName = "Unknown";
  PFont infoFont;
  int sceneWidth;
  int sceneHeight;
  int SEED;
  boolean anime = true;


  void setup() {
    background(255);
    infoFont = createFont("FiraSans-ExtraBold", 16);
    textFont(infoFont, 16);
    SEED = (int)random(1000);
  }
  void draw() {}
  void mousePressed() {}
  void keyPressed() {
    if(key == 'p'){
    anime = !anime;
    if(!anime){
      noLoop();
      println("Animation stopped");
    }else {
      loop();
    }
  }
}

  int getSceneWidth(){
    return sceneWidth;
  }

  int getSceneHeight(){
    return sceneHeight;
  }


  /*
   * Display sketch settings
   *
   */
  void showSettings() {
    pushStyle();
    pushMatrix();
    translate(20, 20);
    String out = "";
    textSize(16);
    textAlign(LEFT);

    out += "--------------------------\n";
    out += "fps: " + nf(frameRate, 0, 1) + "\n";
    out += "current animation: " + author + "\n";

    fill(0,0,255);
    text(out, 0, 0);
    popMatrix();
    popStyle();
  }

  /*
   * Display info for current animation
   *
   */
  void showInfo() {
    pushStyle();
    pushMatrix();
    translate(20, height - 125);
    String info = author + "\n";
    info += sketchName + "\n";
    textSize(16);

    fill(0,0,255);
    text(info, 0, 0);
    popMatrix();
    popStyle();
  }
}
