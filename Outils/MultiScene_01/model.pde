/**
 * Sketch : Empty Scene : Model
 */

class SceneZero extends Scene {


    void setup() {
      author = "Emiline Herveau";
      sketchName = "Piet Mondrian";
      sceneWidth = 800;
      sceneHeight = 600;

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(20, 125);

      popStyle();
      popMatrix();
    }
}
