/**
 * Sketch :
 */


class SceneThree extends Scene {
  float x1, y1;
  //float hasard = 0; //déclaration de variables
  //int SEED;
  float angleStep;

  void setup() {
    author = "Clémence Danne";
    sketchName = "Horst Bartnig";
    sceneWidth = 800;
    sceneHeight = 800;
    //SEED = (int)random(1000);
  }

  void draw() {
    //randomSeed(SEED);
    pushStyle();
    pushMatrix();
    translate(50,50);
    scale(0.87);
    background (250, 250, 250); //couleur du fond (gris léger)
    noFill ();
    strokeWeight (2); //épaisseur des contours 2px
    stroke (100,100,100); //couleur du contour (gris foncé)

    float angle = 0;
    for (int x = 50; x<850; x=x+50) {
      for (int y = 50; y<850; y=y+50) {
      pushMatrix();
      translate(x, y);
      rotate(radians(angle + angleStep)); // rotation de x degres
        ellipse (0,0,40,40);
        line(0, 0, 0, 20);
        line(0, 0, -20, 0);
        popMatrix();
        angle+=30;
        if(anime){
          angleStep += 0.19;
        }

      }
    }
        popMatrix();
        popStyle();
  }


}
