/*
 * :::::::::::::::::
 * Multi-Scene Tool
 * :::::::::::::::::
 *
 * Sketch : MultiScene_01
 * Git    : https://bitbucket.org/acdc_/recode/
 *
 */

// NOTES : 16.01.18: TO_DO
// * - Add timer class for sequencing
// * - Non-repeat random selection
// *

ArrayList <Scene> theScenes;
Scene currentScene = null;
int currentSceneIndex = 10;
boolean timedAnimation = false;

PFont infoFont;

void settings(){
  size(1100, 900, OPENGL);
  smooth(4);
}


void setup(){
  background(0,0,33);
  infoFont = createFont("FiraSans-ExtraBold", 16);
  textFont(infoFont, 16);
  addScenes();
}


void draw(){
  background(0,0,33);
  pushMatrix();
  translate(150, 5);
  currentScene.draw();
  currentScene.showInfo();
  currentScene.showSettings();
  popMatrix();

}


void addScenes(){
  theScenes = new ArrayList<Scene>();
  // add each sketch to the list
  theScenes.add( new SceneOne() );
  theScenes.add( new SceneTwo() );
  theScenes.add( new SceneThree() );
  theScenes.add( new SceneFour() );
  theScenes.add( new SceneFive() );
  theScenes.add( new SceneSix() );
  theScenes.add( new SceneSeven() );
  theScenes.add( new SceneEight() );
  theScenes.add( new SceneNine() );
  theScenes.add( new SceneTen() );
  theScenes.add( new SceneEleven() );
  setupScenes();
}
