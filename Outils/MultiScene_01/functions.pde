/**
 * Various functions
 */



 void setupScenes(){
   for(Scene s : theScenes){
     s.setup();
   }
   currentScene = theScenes.get(currentSceneIndex);
   println("Current scene chosen : "+currentSceneIndex);

 }

 void clearScene(){
   background(0,0,33);
 }

 void keyPressed(){
   currentScene.keyPressed();
   if(keyCode == RIGHT){
     // get next scene
     if (currentSceneIndex <theScenes.size()-1) {
       currentSceneIndex++;
       //println("Scene Index = "+currentSceneIndex);
       currentScene = theScenes.get( theScenes.indexOf(currentScene)+1 );
     }
   }

   if(keyCode == LEFT){
     // get prev scene
     if (currentSceneIndex > 0) {
       currentSceneIndex--;
       //println("Scene Index = "+currentSceneIndex);
       currentScene = theScenes.get(
         theScenes.indexOf(currentScene)-1 );
     }
   }

   if(key == 'r'){
     // get random scene
   }
   // update scene for current display
   //addScenes();
   //setupScenes();
 }
