/**
 * Sketch : Rose-Marie Devignes
 */

class SceneEight extends Scene {
  color couleur [] = {color(62,128,208), color(222,255,0), color (249,141,157), color(87,225,77)};


    void setup() {
      author = "Rose-Marie Devignes";
      sketchName = "John Armleder";
      sceneWidth = 800;
      sceneHeight = 800;

    }

    void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      randomSeed(1);
      translate(110, 100);
      scale(0.7);
      background(#E3DCCA);
      for ( int y=5; y<height; y=y+90) {
        for (int x=10; x<(width/3);x=x+91) {
          noStroke();
          fill(couleur [ int(random(0, 4))]);
          ellipse(x+50, y+70, width/35, width/35);
        }
      }
      for ( int x=-10; x<(width/2.25); x=x+70) {
        for (int y=0; y<(height/2.8); y=y+10) {
          stroke(255, 0, 0);
          strokeWeight(width/40);
          line(x+(width/3), y, x+(width/3), height);
        }
      }

      popStyle();
      popMatrix();
    }
}
