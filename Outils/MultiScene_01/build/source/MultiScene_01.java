import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class MultiScene_01 extends PApplet {

/*
 * :::::::::::::::::
 * Multi-Scene Tool
 * :::::::::::::::::
 *
 * Sketch : MultiScene_01
 * Git    : https://bitbucket.org/acdc_/recode/
 * Note : Read Info tab
 *
 */

// NOTES : add date : TO_DO
// *
// *

ArrayList <Scene> theScenes;
Scene currentScene = null;
int currentSceneIndex = 10;
boolean timedAnimation = false;

PFont infoFont;

public void settings(){
  size(1100, 900, OPENGL);
  smooth(4);
}


public void setup(){
  background(0,0,33);
  infoFont = createFont("FiraSans-ExtraBold", 16);
  textFont(infoFont, 16);
  addScenes();
}


public void draw(){
  background(0,0,33);
  pushMatrix();
  translate(150, 5);
  currentScene.draw();
  currentScene.showInfo();
  currentScene.showSettings();
  popMatrix();


}



public void addScenes(){
  theScenes = new ArrayList<Scene>();
  // add each sketch to the list
  theScenes.add( new SceneOne() );
  theScenes.add( new SceneTwo() );
  theScenes.add( new SceneThree() );
  theScenes.add( new SceneFour() );
  theScenes.add( new SceneFive() );
  theScenes.add( new SceneSix() );
  theScenes.add( new SceneSeven() );
  theScenes.add( new SceneEight() );
  theScenes.add( new SceneNine() );
  theScenes.add( new SceneTen() );
  theScenes.add( new SceneEleven() );
  setupScenes();
}
/*
 * Scene class manager
 *
 */


class Scene {

  String author = "Anonymous";
  String sketchName = "Unknown";
  PFont infoFont;
  int sceneWidth;
  int sceneHeight;
  int SEED;
  boolean anime = true;


  public void setup() {
    background(255);
    infoFont = createFont("FiraSans-ExtraBold", 16);
    textFont(infoFont, 16);
    SEED = (int)random(1000);
  }
  public void draw() {}
  public void mousePressed() {}
  public void keyPressed() {
    if(key == 'p'){
    anime = !anime;
    if(!anime){
      noLoop();
      println("Animation stopped");
    }else {
      loop();
    }
  }
}

  public int getSceneWidth(){
    return sceneWidth;
  }

  public int getSceneHeight(){
    return sceneHeight;
  }


  /*
   * Display sketch settings
   *
   */
  public void showSettings() {
    pushStyle();
    pushMatrix();
    translate(20, 20);
    String out = "";
    textSize(16);
    textAlign(LEFT);

    out += "--------------------------\n";
    out += "fps: " + nf(frameRate, 0, 1) + "\n";
    out += "current animation: " + author + "\n";

    fill(0,0,255);
    text(out, 0, 0);
    popMatrix();
    popStyle();
  }

  /*
   * Display info for current animation
   *
   */
  public void showInfo() {
    pushStyle();
    pushMatrix();
    translate(20, height - 125);
    String info = author + "\n";
    info += sketchName + "\n";
    textSize(16);

    fill(0,0,255);
    text(info, 0, 0);
    popMatrix();
    popStyle();
  }
}
/**
 * Various functions
 */



 public void setupScenes(){
   for(Scene s : theScenes){
     s.setup();
   }
   currentScene = theScenes.get(currentSceneIndex);
   println("Current scene chosen : "+currentSceneIndex);

 }

 public void clearScene(){
   background(0,0,33);
 }

 public void keyPressed(){
   currentScene.keyPressed();
   if(keyCode == RIGHT){
     // get next scene
     if (currentSceneIndex <theScenes.size()-1) {
       currentSceneIndex++;
       //println("Scene Index = "+currentSceneIndex);
       currentScene = theScenes.get( theScenes.indexOf(currentScene)+1 );
     }
   }

   if(keyCode == LEFT){
     // get prev scene
     if (currentSceneIndex > 0) {
       currentSceneIndex--;
       //println("Scene Index = "+currentSceneIndex);
       currentScene = theScenes.get(
         theScenes.indexOf(currentScene)-1 );
     }
   }

   if(key == 'r'){
     // get random scene
   }
   // update scene for current display
   //addScenes();
   //setupScenes();
 }
/*
 ------------------------
 PROJECT : PERSONAL/TEACHING
 29.11.2017

 -------------------------
 Sketch : MULTI-SCENE_00
 Sketch Version : 0.1
 Langauge : Processing
 Version : 3.3

 * Summary : Simple tool for presenting multiple Processing
 *           sketches all in one program.
 *
 * Author :  MWebster 2017 : www.fabprojects.codes
 * Info :    Read more about this on the info tab.

 -------------------------
 LIBRARIES


 -------------------------
 USE
 1). Create a new file/new tab and paste your full sketch.
 2). Enclose your sketch within a class named, Scene+number.
     Eg. SceneOne, SceneTwo...
3). Make sure you extend this class with "...extends Scene"
4). Remove size() function in setup() and add :
      - author = "yourName";
      - sketchName = "SketchName";
      - sceneWidth = 800;
      - sceneHeight = 800;
5). See examples in tabs : sketch_01.pde / sketch_02.pde / sketch_03.pde


 -------------------------
 NOTES
 *

  -------------------------
    LICENSE

    MULTI-SCENE_00
    Copyright (C) 2017 Mark Webster
    www.fabprojects.codes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 // END OF INFO

 */
/**
 * Sketch : Empty Scene : Model
 */

class SceneZero extends Scene {


    public void setup() {
      author = "Emiline Herveau";
      sketchName = "Piet Mondrian";
      sceneWidth = 800;
      sceneHeight = 600;

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(20, 125);

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : MW17
 *
 */

class SceneOne extends Scene {

  public void setup() {
    author = "Cl\u00e9mence";
    sketchName = " ????? ";
    sceneWidth = 800;
    sceneHeight = 800;
    noStroke();
  }

  public void draw() {
    pushStyle();
    rectMode(CORNER);
    background(0,0,33);

    pushMatrix();
    translate(150,150);
    fill(255,0,0);
    rect(-20, -20, 580,600);

    fill(112, 34, 142);
    rect(423, 60, 82, 80);

    fill(125, 23, 25);
    rect(363, 200, 101, 101);

    fill(255, 67, 151);
    rect(363, 300, 121, 118);

    fill(255, 67, 151);
    rect(235, 0, 150, 155);

    fill(125, 23, 25);
    rect(0, 80, 115, 137);

    fill(125, 23, 25);
    rect(75, 0, 40, 85);

    fill(125, 23, 25);
    rect(75, 0, 310, 20);

    fill(125, 23, 25);
    rect(75, 20, 160, 20);

    fill(112, 34, 142);
    rect(23, 120, 90, 97);

    fill(255, 67, 151);
    rect(0, 340, 90, 198);

    fill(255, 67, 151);
    rect(90, 400, 82, 158);

    fill(255, 67, 151);
    rect(117, 480, 245, 65);

    fill(125, 23, 25);
    rect(153, 400, 110, 118);



    // rectangle rouge milieu en bas
    fill(255, 0, 0);
    rect(15, 417, 100, 100);

    fill(67, 85, 20);
    maForme(0, 0, 550, 22);

    // bleu et rose
    fill(255, 67, 151);
    rect(255, 20, 59, 60);

    pushMatrix();
    fill(131, 208, 245);
    rotate(radians(90));
    translate(-235, -332);
    maForme(255, 23, 55, -30);
    popMatrix();


    // bleu f et rouge et bordeau

    fill(255, 0, 0);
    rect(75, 55, 160, 162);

    //b
    fill(125, 23, 25);
    rect(75, 55, 40, 65);

    //v
    fill(112, 34, 142);
    rect(75, 120, 40, 97);

    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(-191, -255);
    maForme(255, 23, 155, 30);
    popMatrix();

    // Bleu moyen
    //rouge
    fill(255, 0, 0);
    rect(253, 150, 133, 110);
    //b
    noStroke();
    fill(125, 23, 25);
    rect(365, 200, 40, 60);

    //rose


    fill(255, 67, 151);
    rect(253, 99, 133, 57);

    //rouge petit
    fill(255, 0, 0);
    rect(385, 95, 20, 105);

    fill(9, 98, 198);
    maForme(255, 100, 150, -15);
    fill(125, 23, 25);
    rect(365, 280, 98, 20);
    //rouge 1


    fill(255, 0, 0);
    rect(245, 275, 120, 125);


    fill(255, 0, 0);
    rect(243, 380, 122, 100);
    fill(255, 67, 151);
    rect(365, 300, 100, 118);

    fill(125, 23, 25);
    rect(243, 400, 20, 80);

    fill(255, 0, 0);
    rect(365, 420, 100, 60);


    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(220, 20);
    maForme2(55, -482, 205, -15);
    popMatrix();



    // bleu moyen
    //rose

    fill(255, 67, 151);
    rect(135, 435, 20, 80);

    fill(125, 23, 25);
    rect(153, 435, 60, 80);

    pushMatrix();
    fill(9, 98, 198);
    rotate(radians(90));
    translate(-19, -25);
    maForme(463, -187, 75, 30);
    popMatrix();


    //le dernier
    //rouge 1
    fill(255, 0, 0);
    rect(34, 235, 180, 105);

    //rouge 2


    fill(255, 0, 0);
    rect(94, 340, 120, 60);

    //rose
    fill(255, 67, 151);
    rect(34, 340, 80, 76);
    //rose 2

    fill(255, 67, 151);
    rect(114, 400, 40, 20);

    fill(125, 23, 25);
    rect(154, 400, 60, 20);
      pushMatrix();
      fill(101, 200, 245);
      maForme(35, 240, 180, -15);
      popMatrix();
      popMatrix();
      popStyle();
  }


  public void maForme(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    noStroke();
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }

  public void maForme2(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }
}
/**
 * Sketch : Margot
 *
 */

class SceneTwo extends Scene {

  public void setup() {
    author = "Emiline";
    sketchName = "Franck Stella";
    sceneWidth = 800;
    sceneHeight = 800;
//    strokeCap(ROUND);
}

/////////////////////////// DRAW ////////////////////////////

public void draw() {
  rectMode(CORNER);
    pushStyle();
    background(200);
    pushMatrix();
    translate(150, 250);
    fill(231, 230, 225);
    noStroke();
    rect(-50, -55, 645, 450);
    strokeWeight(7.2f);
    strokeCap(ROUND);
    stroke(0xff393738);

      line(0, 26, 0, 282);
      line(560, 54, 560, 309);

    for (int x=0; x<100; x=x+100) {
      for (int y=-20; y<240; y=y+10) {
        line(x, y+49, x+102, y+0);
        line(x+99, y+0, x+216, y+69);
        line(x+214, y+69, x+351, y+69);
        line(x+349, y+69, x+461, y+129);
        line(x+459, y+129, x+561, y+75);
      }
    }
  popMatrix();
  popStyle();
  }

}
/**
 * Sketch :
 */


class SceneThree extends Scene {
  float x1, y1;
  //float hasard = 0; //d\u00e9claration de variables
  //int SEED;
  float angleStep;

  public void setup() {
    author = "Cl\u00e9mence Danne";
    sketchName = "Horst Bartnig";
    sceneWidth = 800;
    sceneHeight = 800;
    //SEED = (int)random(1000);
  }

  public void draw() {
    //randomSeed(SEED);
    pushStyle();
    pushMatrix();
    translate(50,50);
    scale(0.87f);
    background (250, 250, 250); //couleur du fond (gris l\u00e9ger)
    noFill ();
    strokeWeight (2); //\u00e9paisseur des contours 2px
    stroke (100,100,100); //couleur du contour (gris fonc\u00e9)

    float angle = 0;
    for (int x = 50; x<850; x=x+50) {
      for (int y = 50; y<850; y=y+50) {
      pushMatrix();
      translate(x, y);
      rotate(radians(angle + angleStep)); // rotation de x degres
        ellipse (0,0,40,40);
        line(0, 0, 0, 20);
        line(0, 0, -20, 0);
        popMatrix();
        angle+=30;
        if(anime){
          angleStep += 0.19f;
        }

      }
    }
        popMatrix();
        popStyle();
  }


}
/**
 * Sketch : Sophie Mialon
 */

 class SceneFour extends Scene {

   public void setup() {
     author = "Sophie Mialon";
     sketchName = "Bridget Riley";
     sceneWidth = 800;
     sceneHeight = 800;
   }

   public void draw() {
     background(200);
     rectMode(CORNER);
     randomSeed(1);
     pushMatrix();
     translate(20, 125);
     drawQuad(500, 700);
     makeShape(760, 530);
     popMatrix();
   }
   public void makeShape(int w, int h) {
     fill(230);
     noStroke();
     rect(0, 0, w, 40);
     rect(0, 0, 50, h);
     rect(710, 0, 50, h);
     rect(0, 490, w, 40);
   }

   public void drawQuad(int docHeight, int docWidth) {
     int[] colorList = {color(237, 216, 189), color(18, 152, 196), color(237, 148, 32), color(85, 129, 57), color(232, 95, 81), color(129, 150, 203), color(246, 231, 212), color(2, 2, 2)};
     for (int y = 40; y < docHeight+20; y+=20) {
       for (int x = 38; x < docWidth; x+=38) {
         //rect(x, y, 40, 25);
         int randomColor = colorList[(int)random(colorList.length) ];
         noStroke();
         fill(randomColor);
         quad(x, y, x+38, y-20, x+38, y, x, y+24);
       }
     }
   }
 }
/**
 * Sketch : Robin
 */

 class SceneFive extends Scene {

   float x, y, a, b, e, f, h, k, l, m, n, o;
   int r, v, g;
   int SEED;
   float taille = 30;
   int timer = 0;



 public void setup () {
   a=0;
   b=0;
   background(0);
   noFill();
   SEED = (int)random(1000);
   author = "Robin";
   sketchName = "Vera Molnar";
   sceneWidth = 800;
   sceneHeight = 800;
 }


 public void draw() {
    rectMode(CORNER);
     pushMatrix();
     translate(50,60);
     scale(0.93f);
     //background(0,33);
     fill(0, 7);
     noStroke();
     rect(0,0,width, height);
     strokeWeight(1.3f); //\u00e9paisseur du trait
     randomSeed(SEED);

     for (int j =0; j<36; j=j+1) { //boucle pour les 36 carr\u00e9s
       x=20; //valeur du x \u00e0 la base
       y=120; //valeur du y \u00e0 la base

       r=PApplet.parseInt(random(3, 255)); // int est l\u00e0 pour arrondir parce que impossible d'avoir une valeur d\u00e9cimale
       v=PApplet.parseInt(random(3, 233));
       g=PApplet.parseInt(random(3, 255));


       stroke(r, v, g); // couleur du contour


       pushMatrix(); //d\u00e9but du calque
       translate(a, b);

       for ( int i = 0; i<10; i=i+1 ) { //lancement de la boucle, tourne 10x
         x=x+4; //le carr\u00e9 du milieu sera 4 px + \u00e0 droite
         y=y-4; //le carr\u00e9 du milieu sera 4 px + bas
         quad (x+e, x+f, x+h, y+k, y+l, y+m, y+n, x+o); //cr\u00e9e les points du carr\u00e9


         e=random(-taille, taille);
         f=random(-taille, taille);
         h=random(-taille, taille);
         k=random(-taille, taille);
         l=random(-taille, taille);
         m=random(-taille, taille);
         n=random(-taille, taille);
         o=random(-taille, taille);

         println(e);
       }
       popMatrix(); //fin du calque


       a=a+120;

       if (a>600) //retour \u00e0 la ligne des carr\u00e9s
       {
         b=b+120;
         a= 0;
       }

       if (b > 600 ) {
         b=0;
       }
     }
     timer++;
     if(timer>=100){
      timer = 0;
       SEED = (int)random(1000);
     }
     popMatrix();
   }
 }
/**
 * Sketch : Robin
 */

 class SceneSix extends Scene {


   public void setup(){
     author = "Hugon_Yseult";
     sketchName = "Grant Wiggins";
   }

   public void draw(){
     rectMode(CORNER);
     pushMatrix();
     translate(130,140);
     background(0xff708186);
     fill(255);
     noStroke();
     rect(0, 258.5f, 509, 258.5f);


   for (int i = 210; i < 300; i+=10) {
     fill(0xffFF368D);
     rect(125, i, 385, 5);
   }

   for (int i = 215; i < 295; i+=10) {
     fill(0xff6FA3D8);
     rect(125, i, 385, 5);
   }



   for (int i = 230; i<400; i+=92) {
     for (int j = 75; j<400; j+=92) {

      pushMatrix();
       translate(j, i);
       for (int k = 0; k < 48; k+=1) {
         fill(255,0,0);
         rect(k, k, 46, 46);
         if (k == 47) {
           fill( 0xffFF00E2);
           rect(0, 0, 46, 46);
         }
       }
      popMatrix();



     }
   }

   for (int i = 276 ; i<400; i+=92) {
     for (int j = 122; j<400; j+=92) {

       pushMatrix();
       translate(j, i);
       for (int k = 0; k < 48; k+=1) {
         fill(255,0,0);
         rect(k, k, 46, 46);
         if (k == 47 ) {
           fill( 0xffFF00E2);
           rect(0, 0, 46, 46);

         }
       }
       popMatrix();

     }
   }

   for (int y = 168 ; y<400 ; y+=92){
     fill(0xffFF00E2);
     rect(y,322,46,46);
   }

   for (int y = 122 ; y<400 ; y+=92){
     fill(255,0,0);
     rect(y,322,46,46);
   }

   for (int y = 0; y < 200; y = y + 40) {
    for (int i = 125 ; i < 200 ; i= i +40){
       fill(0xffFF368D);
       rect(i, 10+y, 385, 5 );
       fill(0xff6FA3D8);
       rect(i, 15+y, 385, 5 );
       fill(0xffFF368D);
       rect(60+i, 20+y, 385, 5 );
       fill(0xff6FA3D8);
       rect(60+i, 25+y, 385, 5 );
       fill(0xffFF368D);
       rect(120+i, 30+y, 385, 5 );
       fill(0xff6FA3D8);
       rect(120+i, 35+y, 385, 5 );
       fill(0xffFF368D);
       rect(180+i, 40+y, 385, 5 );
       fill(0xff6FA3D8);
       rect(180+i, 45+y, 385, 5 );
     }
    }
    fill(0xff708186);
    rect(509, 0, 300, 400);
    popMatrix();
   }
 }
/**
 * Sketch : Emeline Herveau
 */

class SceneSeven extends Scene {

    float epaisseur = 0.1f;
    float phi = (sqrt(5)+1)/2;
    int SEED;
    int timer;

    public void setup() {
      author = "Emiline Herveau";
      sketchName = "Piet Mondrian";
      sceneWidth = 800;
      sceneHeight = 600;
      background(255);
      SEED = (int)random(1000);
      timer = 0;
    }

    public void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      translate(20, 125);

      randomSeed(SEED);
      translate(sceneHeight/phi, 0);

      scale(sceneHeight*0.85f);
      for (float i=0; i<12; i++) {
        int poss = 0;
        random(poss);

        int hasard = PApplet.parseInt(random(0, 4));

        int couleur [] = {color(0xffFF0329), color(0xff1956F7), color(0xffFEFF00), color(0xffFFFFFF)};
        fill(couleur[hasard]);

        stroke(0);
        strokeWeight(0.05f);

        rect(0, 0, 1, 1);
        epaisseur = epaisseur  - 0.005f;
        scale(1/phi);
        rotate(PI/2);
        translate(1/phi, 0);
      }
      timer++;
      if (timer>=30) {
        timer = 0;
        SEED = (int)random(1000);
      }
      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Rose-Marie Devignes
 */

class SceneEight extends Scene {
  int couleur [] = {color(62,128,208), color(222,255,0), color (249,141,157), color(87,225,77)};


    public void setup() {
      author = "Rose-Marie Devignes";
      sketchName = "John Armleder";
      sceneWidth = 800;
      sceneHeight = 800;

    }

    public void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      randomSeed(1);
      translate(110, 100);
      scale(0.7f);
      background(0xffE3DCCA);
      for ( int y=5; y<height; y=y+90) {
        for (int x=10; x<(width/3);x=x+91) {
          noStroke();
          fill(couleur [ PApplet.parseInt(random(0, 4))]);
          ellipse(x+50, y+70, width/35, width/35);
        }
      }
      for ( int x=-10; x<(width/2.25f); x=x+70) {
        for (int y=0; y<(height/2.8f); y=y+10) {
          stroke(255, 0, 0);
          strokeWeight(width/40);
          line(x+(width/3), y, x+(width/3), height);
        }
      }

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Empty Scene : Model
 */

class SceneNine extends Scene {


    public void setup() {
      author = "Rose-Marie Devignes";
      sketchName = "Vera Molnar";
      sceneWidth = 800;
      sceneHeight = 800;

    }

    public void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      randomSeed(SEED);
      translate(90, 125);
      scale(0.73f);
      background(255, 255, 255);
      fill(0);
      stroke(0);
      strokeWeight(2);

      for (int y=0; y<sceneHeight/3; y+=40) {
        for (int x=0; x<sceneWidth; x+=40) {
          float angleRandom=random(-75, 75);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          rect(0, -10, 40, 10, 200);
          popMatrix();
        }
      }

      for (int y=sceneHeight/3; y<sceneHeight-370; y+=40) {
        for (int x=0; x<sceneWidth; x+=50) {
          float angleRandom=random(-75, 75);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          rect(0, -10, 40, 10, 200);
          rect(5,20, 40, 10, 200);
          popMatrix();
        }
      }
      for (int y=sceneHeight-350; y<sceneHeight; y+=40
        ) {
        for (int x=0; x<sceneWidth; x+=40) {
          float angleRandom=random(-180, 180);
          pushMatrix();
          translate(x, y);
          rotate (radians(angleRandom));
          rect(0, -10, 40, 10, 200);
          rect(5, 5, 40, 10, 200);
          rect(10, 20, 40, 10, 200);
          popMatrix();
        }



      }
      noFill();
      strokeWeight( 50) ;
        stroke(255);
        rect(-60,-60, sceneWidth+100, sceneHeight+100);

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Hans Kuiper
 *
 */

class SceneTen extends Scene {

  float x1;
  float y1;
  float x2;
  float y2;
  float angleStep;

  public void setup() {
    author = "Emiline";
    sketchName = "Hans Kuiper";
    sceneWidth = 800;
    sceneHeight = 800;
  }
    public void draw(){
      background(240);
      fill(0);
      rectMode(CENTER);
      pushMatrix();
      translate(150, 165);
      scale(1.3f);
      float angle = 0;

      for (int y= 2; y < 410; y = y + 40) {
        for (int x=2; x< 410; x = x + 40) {
          //float m = map(mouseX, 0, width, 0.001, 0.1);
          //float angle = noise(x * m, y * m) * 3;

          pushMatrix(); // matrix servent \u00e0 cr\u00e9er des calques
          translate(x, y); // bouger mes nouveaux calques
          rotate(radians(angle + angleStep)); // rotation de x degres

          rect(0, 0, 35, 12);
          rect(0, 0, 12, 35);
          popMatrix();
          //deg = deg + 9;
          angle+=9;
        }

      }
      popMatrix();
      if(anime){
        angleStep += 0.9f;
      }
  }

}
/**
 * Sketch : JODI
 *
 */

class SceneEleven extends Scene {


    PFont font;
    float x, y;
    char[] chars ={'\u2588', '\u2588', '\u2593', '\u2592', '\u2591', '#', '\u2261', '%', '$', '@', '&'};
    char c;
    int index;
    int marge = 60;
    int seed;

    public void setup() {
      author = "CL\u00e9zio Descluse";
      sketchName = "Jodi";
      sceneWidth = 800;
      sceneHeight = 800;
      font = createFont("font.ttf", 50);
      seed = (int)random(1000);
    }


    public void draw() {
      background(0);
      pushStyle();
      rectMode(CENTER);
      textFont(font);
      textAlign(CENTER, CENTER);
      randomSeed(seed);
      pushMatrix();
      translate(0, 100);
      scale(0.7f);
      for (int y=marge; y<height-marge; y+=30) {
        for (int x=marge; x<width-marge/2; x+=30) {
          textFont(font);
          int dice = (int)random(8);
          if (dice<2) {
            index = (int)random(33, 127);
            c = PApplet.parseChar(index);
            fill(255);
            text(c, x, y);
          } else if (dice>2 && dice<4) {
            int theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme1(x, y, longeur, theColour);
          } else if (dice>4 && dice<6) {
            int theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme2(x, y, longeur, theColour);
          } else if (dice>6 && dice<8) {
            int theColour = color(random(100, 255), random(5, 255), random(0, 255));
            float longeur = random(5, 100);
            forme3(x, y, longeur, theColour);
            x=x-15;
          } else {
          }
        }
      }
      popMatrix();
      popStyle();
    }

    public void keyPressed() {
        seed = (int)random(0, 1000);
    }

    public void forme1(float _x, float _y, float _len, int _c) {
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, _len, 30);
      popMatrix();
      popMatrix();
    }

    public void forme2(float _x, float _y, float _len, int _c) {
      int dice = (int)random(6);
      if (dice<3) {
        int theColour = color(random(100, 255), random(5, 255), random(0, 255));
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, _len, 30);
        popMatrix();
        popMatrix();
      }
    }



    public void forme3(float _x, float _y, float _len, int _c) {
      int dice = (int)random(6);
      if (dice<3) {
        int theColour = color(0);
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, 10, 10);
        popMatrix();
        popMatrix();
      } else {
        int theColour = color(255);
        fill(theColour);
        noStroke();
        pushMatrix();
        translate(_x, _y);
        pushMatrix();
        translate(5, 6);
        rect(0, 0, 10, 10);
        popMatrix();
        popMatrix();
      }
}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--hide-stop", "MultiScene_01" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
