/**
 * Various functions
 */

   // variables for generateRandomSequence function.
   int currentRandScene;
   int sceneNum;
   int[] randScene;

/**
 * Init scenes : size & other setup configs
 */

 void setupScenes(){
   currentScene = theScenes.get(currentSceneIndex);
   println("Current scene chosen : "+currentSceneIndex);
   for(Scene s : theScenes){
     s.setSceneDimensions();
     s.setup();
   }
 }

 void clearScene(){
   background(0,0,33);
 }

 void keyPressed(){
   // get next scene
   if(keyCode == RIGHT){
     if (currentSceneIndex <theScenes.size()-1) {
       currentSceneIndex++;
       currentScene = theScenes.get( theScenes.indexOf(currentScene)+1 );
     }
     //update timer
     myTimer.reset();
   }
   // get prev scene
   if(keyCode == LEFT){
     if (currentSceneIndex > 0) {
       currentSceneIndex--;
       currentScene = theScenes.get(
         theScenes.indexOf(currentScene)-1 );
     }
     //update timer
     myTimer.reset();
   }
   if(key == 'a'){
     autoMode = !autoMode;
     setup();
   }
   if(key == 'i'){
    showSettings = !showSettings;
   }
   if(key == 'd'){
    showCode = !showCode;
   }
 }

// GENERATES A NEW RANDOM SEQUENCE FOR ANIMATION PLAY.
// EACH SEQUENCE IS UNIQUE & NEVER REPEATS AN ANIMATION ;–)
// SEE checkRandom() FUNCTION BELOW FOR IMPLEMENTING THIS.
void generateRandomSequence() {
  currentRandScene = 0;
  int scenesMax = theScenes.size();
  sceneNum = scenesMax;
  println("Total Scenes = "+sceneNum); // DEBUG
  randScene = new int[ sceneNum -1 ];

  int index = 0;
  while (index<randScene.length) {
    int num = (int)random(sceneNum);
    if (checkRandom(num)) {
      randScene[index] = num;
      index++;
    }
  }
  println(" \n/////////////////////////////////////////////////////////////////");
  for (int i = 0; i < randScene.length; ++i) {
    println("Random Scene Sequence ="+randScene[i]);
  }
   println(" \n/////////////////////////////////////////////////////////////////");
}
////////////////////////////////////////////////////////////////////
void resetAll() {
  background(0); // clear screen
  //displayScene = false;
  myTimer.reset();
  myTimer = new Timer(20000); // 20 second timer

  currentSceneIndex = randScene[ currentRandScene ];

  currentScene = theScenes.get(currentSceneIndex);
  println("Current scene chosen : "+currentSceneIndex);

  // setup Scenes
  for (Scene theScene : theScenes) {
    theScene.setSceneDimensions();
    theScene.setup();
  }

  // CHECK TO SEE IF CURRENT RANDOM SEQUENCE HAS FINISHED. IF YES, GENERATE ANOTHER NEW SEQUENCE
  if (currentRandScene<randScene.length-1) {
    currentRandScene++;
  } else {
    generateRandomSequence();
  }
}

/**
 * Method for checking same int numbers
 * @param   num   the int number to check
 * @return        returns true or false
 */
boolean checkRandom(int num) {
  for (int i=0; i<randScene.length; i++) {
    if (num == randScene[i]) return false;
    if (num == 0) return false; // never choose zero (could be handy !)
  }
  return true;
}
