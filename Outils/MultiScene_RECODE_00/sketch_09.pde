/**
 * Sketch : Rose-Marie Devignes
 */

class SceneNine extends Scene {


  void setup() {
    student = "Emiline Herveau";
    artist = "Vera Molnar";
    title = "Un, Deux, Trois";
    sketchCode = "sketch_09.pde";
    code = loadPde(sketchCode);
  }

  void draw() {
    rectMode(CORNER);
    pushMatrix();
    pushStyle();
    randomSeed(1); // ADD the seed somewhere > animations !
    translate(90, 125);
    scale(0.73);
    background(255, 255, 255);
    fill(0);
    stroke(0);
    strokeWeight(2);

    for (int y=0; y<sceneHeight/3; y+=40) {
      for (int x=0; x<sceneWidth; x+=40) {
        float angleRandom=random(-75, 75);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        popMatrix();
      }
    }

    for (int y=sceneHeight/3; y<sceneHeight-370; y+=40) {
      for (int x=0; x<sceneWidth; x+=50) {
        float angleRandom=random(-75, 75);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        rect(5, 20, 40, 10, 200);
        popMatrix();
      }
    }
    for (int y=sceneHeight-350; y<sceneHeight; y+=40
      ) {
      for (int x=0; x<sceneWidth; x+=40) {
        float angleRandom=random(-180, 180);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        rect(5, 5, 40, 10, 200);
        rect(10, 20, 40, 10, 200);
        popMatrix();
      }
    }
    noFill();
    strokeWeight( 50) ;
    stroke(255);
    rect(-60, -60, sceneWidth+100, sceneHeight+100);

    popStyle();
    popMatrix();
  }
}