/**
 * Sketch : DucThuan Nguyen
 */

class SceneTwenty extends Scene {

      float x1 [];
      float x2 [];
      float y1 [];
      float y2 [];

    void setup() {
      student = "DucThuan Nguyen";
      artist = "????";
      title = "??????";
      sketchCode = "sketch_20.pde";
      code = loadPde(sketchCode);
        x1 = new float [10];
         y1 = new float [10];
         x2 = new float [10];
         y2 = new float [10];
         for (int k=1; k<10; k++){
           x1[k]=random (0,600);
           x2[k]=random (0,600);
           y1[k]=random (0,600);
           y2[k]=random (0,600);
         }

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/6, screenSizeH/8);

      background (200,200,200);
       noStroke();
       for (int l=1; l<20; l++){
         fill (0,0,0,4);
         ellipse (screenSizeW/4,screenSizeW/6+l,315,315);
       }
       fill (210);

       ellipse (screenSizeW/4,screenSizeW/6,310,310);

       for (int j=1; j<10; j++){
         for (int i=3; i<30; i++) {
           strokeWeight(i);
           stroke (255,255,255,4);
           line (x1[j],y1[j],x2[j],y2[j]);
         }
         strokeWeight(2);
         stroke (255,255,255,255);
         line (x1[j],y1[j],x2[j],y2[j]);
       }
       for (int k=1; k<10; k++){
         x1[k]=x1[k]+random (-15,15);
         x2[k]=x2[k]+random (-15,15);
         y1[k]=y1[k]+random (-15,15);
         y2[k]=y2[k]+random (-15,15);
       }

      popStyle();
      popMatrix();
    }
}
