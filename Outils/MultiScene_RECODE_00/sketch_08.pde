/**
 * Sketch : Rose-Marie Devignes
 */

class SceneEight extends Scene {
    
    color couleur [] = {color(62,128,208), color(222,255,0), color (249,141,157), color(87,225,77)};
    
    void setup() {
      student = "Rose-Marie Devignes";
      artist = "John Armleder";
      title = "Cast Iron";
      sketchCode = "sketch_08.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
    rectMode(CORNER);
    background(0);
      pushMatrix();
      pushStyle();
      randomSeed(1);
      translate(sceneWidth/7, sceneHeight/8); 
      //scale(0.7);
      
      fill(#E3DCCA);
      rect(-30,-30, sceneWidth, sceneHeight);
      for ( int y=5; y<sceneHeight; y=y+90) {
        for (int x=10; x<sceneWidth-sceneWidth/1.8;x+=100) {
          noStroke();
          fill(couleur [ int(random(0, 4))]);
          ellipse(x+50, y+70, sceneWidth/35, sceneWidth/35);
        }
      }
      
      strokeWeight(sceneWidth/40);
      for ( int x=-10; x<(sceneWidth); x=x+70) {
        for (int y=-25; y<(sceneHeight); y=y+10) {
          stroke(255, 0, 0);         
          line(x+(sceneWidth/3), y, x+(sceneWidth/3), sceneHeight);
        }
      }

      popStyle();
      popMatrix();
    }
}