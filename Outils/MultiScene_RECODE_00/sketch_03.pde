/**
 * Sketch : Emiline Herveau
 */

class SceneThree extends Scene {


  void setup() {
    student = "Emiline Herveau";
    artist = "Franck Stella";
    title = "Mas O Menos";
    sketchCode = "sketch_03.pde";
    code = loadPde(sketchCode);
  }

  void draw() {

    pushStyle();
    rectMode(CORNER);
    background(200);
    pushMatrix();
    translate(sceneWidth/3, sceneHeight/4);
    fill(231, 230, 225);
    noStroke();
    rect(-50, -55, 645, 450);
    strokeWeight(7.2);
    strokeCap(ROUND);
    stroke(#393738);

    line(0, 26, 0, 282);
    line(560, 54, 560, 309);

    for (int x=0; x<100; x=x+100) {
      for (int y=-20; y<240; y=y+10) {
        line(x, y+49, x+102, y+0);
        line(x+99, y+0, x+216, y+69);
        line(x+214, y+69, x+351, y+69);
        line(x+349, y+69, x+461, y+129);
        line(x+459, y+129, x+561, y+75);
      }
    }
    popMatrix();
    popStyle();
  }
}
