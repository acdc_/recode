/**
 * Sketch : Michel Ludivine
 */

class SceneFifteen extends Scene {

    float taille =120;// taille des carré millieu
    float taille2 =300; //taille  carré exterieur
    float x = 150; //taille carré horizontal
    float y =150; // taille carré vertical


    void setup() {
      student = "Michel Ludivine";
      artist = "Jean Pierre Yvaral";
      title = "Quadrature E";
      sketchCode = "sketch_15.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/5, screenSizeH/8);
      background(0);
      noFill();
      stroke(#050355);
      strokeWeight(4);

      rectMode(CENTER);
      // . . . your code here

      for (int i = 5; i<37; i+=4) {
        translate(-10.8, 14.4); // déplacer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        rect(177, 1, taille, taille);
        taille = taille -4; // taille de chaque carré-2
      }

      float taille3 =120;// taille des carré millieu
      float taille4 =300; //taille  carré exterieur
      float a = 537; //taille carré horizontal
      float b =35; // taille carré vertical

      noFill();
      stroke(#050355);
      strokeWeight(4);

      //rectMode(CENTER); // carré exterieur centré


      for (int i = 1; i<30; i+=3) {
        rect(a, b, taille4 ,taille4);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        a = a -3;
        b= b -3;
       taille4 = taille4 -23; // taille de chaque carré-20
      }

      //rectMode(CORNER); // carré exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(-10.8, -11); // déplacer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        rect(564, 63, taille3, taille3);
        taille3 = taille3 -4; // taille de chaque carré-2
      }

      float taille5 =120;// taille des carré millieu
      float taille6 =300; //taille  carré exterieur
      float c = 623; //taille carré horizontal
      float d =424; // taille carré vertical

      noFill();
      stroke(#050355);
      strokeWeight(4);

      //rectMode(CENTER); // carré exterieur centré


      for (int i = 1; i<30; i+=3) {
        rect(c, d, taille6 ,taille6);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        c = c +3;
        d= d -3;
       taille6 = taille6 -23; // taille de chaque carré-20
      }

    //  rectMode(CORNER); // carré exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(+14.5, -10.5); // déplacer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        rect(473, 450, taille5, taille5);
        taille5 = taille5 -4; // taille de chaque carré-2
      }

      float taille7 =120;// taille des carré millieu
      float taille8 =300; //taille  carré exterieur
      float e = 207; //taille carré horizontal
      float f =508; // taille carré vertical

      noFill();
      stroke(#050355);
      strokeWeight(4);

      //rectMode(CENTER); // carré exterieur centré


      for (int i = 1; i<30; i+=3) {
        rect(e, f, taille8 ,taille8);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        e = e +3;
        f= f +3;
       taille8 = taille8 -23; // taille de chaque carré-20
      }

    //  rectMode(CORNER); // carré exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(+14.5, +13.9); // déplacer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // dégradé bleu
        strokeWeight(4);
        rect(57, 360, taille7, taille7);
        taille7 = taille7 -4; // taille de chaque carré-2
}

      popStyle();
      popMatrix();
    }
}
