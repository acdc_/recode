/**
 * Sketch : Hugon_Yseult
 */

class SceneSix extends Scene {

  float OffsetX = 0;

  void setup() {
    student = "Hugon_Yseult";
    artist = "Grant Wiggins";
    title = "The Lake A Lilac Cube";
    sketchCode = "sketch_06.pde";
    code = loadPde(sketchCode);
  }
  void draw() {
    rectMode(CORNER);
    pushMatrix();
    pushStyle();
    translate(sceneWidth/3.7, sceneHeight/8);
    background(#708186);
    fill(255);
    noStroke();
    rect(0, 258.5, 509, 258.5);


    for (int i = 210; i < 300; i+=10) {
      fill(#FF368D);
      rect(125, i, 385, 5);
    }

    for (int i = 215; i < 295; i+=10) {
      fill(#6FA3D8);
      rect(125, i, 385, 5);
    }



    for (int i = 230; i<400; i+=92) {
      for (int j = 75; j<400; j+=92) {

        pushMatrix();
        translate(j, i);
        for (int k = 0; k < 48; k+=1) {
          fill(255, 0, 0);
          rect(k, k, 46, 46);
          if (k == 47) {
            fill( #FF00E2);
            rect(0, 0, 46, 46);
          }
        }
        popMatrix();
      }
    }

    for (int i = 276; i<400; i+=92) {
      for (int j = 122; j<400; j+=92) {

        pushMatrix();
        translate(j, i);
        for (int k = 0; k < 48; k+=1) {
          fill(255, 0, 0);
          rect(k, k, 46, 46);
          if (k == 47 ) {
            fill( #FF00E2);
            rect(0, 0, 46, 46);
          }
        }
        popMatrix();
      }
    }

    for (int y = 168; y<400; y+=92) {
      fill(#FF00E2);
      rect(y, 322, 46, 46);
    }

    for (int y = 122; y<400; y+=92) {
      fill(255, 0, 0);
      rect(y, 322, 46, 46);
    }
    OffsetX = 0;
    for (int y = 0; y < 200; y = y + 40) {
      if(anime) {
        OffsetX = sin((y*10.0)+frameCount*0.015)*60;
      }else {
        OffsetX = 0;
      }
      for (int i = 125; i < 200; i= i +40) {
        pushMatrix();
        translate(i+OffsetX, 0);
        fill(#FF368D);
        rect(0, 10+y, 385, 5 );
        fill(#6FA3D8);
        rect(0, 15+y, 385, 5 );
        fill(#FF368D);
        rect(60, 20+y, 385, 5 );
        fill(#6FA3D8);
        rect(60, 25+y, 385, 5 );
        fill(#FF368D);
        rect(120, 30+y, 385, 5 );
        fill(#6FA3D8);
        rect(120, 35+y, 385, 5 );
        fill(#FF368D);
        rect(180, 40+y, 385, 5 );
        fill(#6FA3D8);
        rect(180, 45+y, 385, 5 );
        popMatrix();
      }
    }
    fill(#708186);
    rect(509, 0, 300, 400);
    popMatrix();
    popStyle();
  }
}
