/**
 * Sketch : Evan Lelièvre
 */

class SceneTwelve extends Scene {

    float deg = 0;

    void setup() {
      student = "Evan Lelièvre";
      artist = "Julio Le Parc";
      title = "Modulation 1159";
      sketchCode = "sketch_12.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/4, screenSizeH/6);
      fill(255);
      noStroke();
      background(0);

      float opacite = 160;
      for(int i = 0; i < 6; i++) {
        pushMatrix();
        translate(390,230);
        rotate(radians(deg));
        fill(200, opacite);
        rect(60,0,120,120);

        deg = deg+18;
        popMatrix();
      }

      deg = 253;
      for(int i = 0; i < 5; i++) {
       pushMatrix();
       translate(270,475);
       rotate(radians(deg));
       fill(200, opacite);
       rect(60,0,120,120);
       popMatrix();
       deg = deg-18;
      }

      popStyle();
      popMatrix();
    }
}
