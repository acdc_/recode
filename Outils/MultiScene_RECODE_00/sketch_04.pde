/**
 * Sketch : Clémence Danne
 */

class SceneFour extends Scene {

  float angleStep = 0.05;
  float hasard = 0.85; //déclaration de variables

  void setup() {
    student = "Clémence Danne";
    artist = "Horst Bartnig";
    title = "Ohne Titel";
    sketchCode = "sketch_04.pde";
    code = loadPde(sketchCode);
  }

  void draw() {
    pushStyle();
    pushMatrix();
    translate(sceneWidth/3.8, sceneHeight/14);
    scale(0.80);
    background(#E8E9EA);
    noFill ();
    strokeWeight (2); //épaisseur des contours 2px
    stroke (100, 100, 100); //couleur du contour (gris foncé)
    drawDessin();
    popMatrix();
    popStyle();

  }
  void drawDessin() {
  float angle = 0;
  float angleChange = 0;
  for (int x = 50; x<850; x=x+50) {    // boucle for, x commence placé à 50px, il doit arriver à 850px, on incrémente la position de 50 pixels
    for (int y = 50; y<850; y=y+50) {  //boucle for, y commence placé à 50, il doit arriver à 850, on incrémente la position de 50 pixels

      pushMatrix(); //ouverture d'un nouveau calque (à chaque nouvelle boucle, un nouveau calque)
      translate(x, y); //changement de la position d'origine du calque (0,0) aux valeurs x et y déterminées par la boucle.

      //rotate(radians(hasard)); //
      rotate(radians(angle + angleChange)+hasard);

      noFill (); //pas de remplissage couleur de la forme qui va être générée
      smooth(); //adoucir les contours des formes générées
      strokeWeight (2); //épaisseur des contours 2px
      stroke (100, 100, 100); //couleur du contour (gris foncé)
      ellipse (0, 0, 40, 40); //générer une ellipse par boucle. La position (0,0) permet d'avoir le cercle positionné en haut à gauche du calque généré. La position du calque changeant à chaque nouvelle boucle, le cercle change aussi de position.
      line(0, 0, 0, 20); //générer une ligne par boucle. Même fonctionnement
      line(0, 0, -20, 0); //générer une ligne par boucle. Même fonctionnement

      popMatrix(); //fermeture du calque généré
      if (anime) {
        hasard = sin((y*2.005)+frameCount*0.005)*45; // à chaque nouvelle boucle, la valeur "hasard" augmente de 0.85, les calques tournent donc de 0.85° à chaque nouvelle boucle.
      }else {
        hasard = 0;
      }
    }
    angleChange = x*0.23;
  }
}
}
