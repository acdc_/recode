/**
 * Sketch : Empty Scene : Model
 */

class SceneTen extends Scene {

  float x1;
  float y1;
  float x2;
  float y2;
  float angleStep;


  void setup() {
    student = "Elisabeth Musielak";
    artist = "Hans Kuiper";
    title = "???????";
    sketchCode = "sketch_10.pde";
    code = loadPde(sketchCode);
  }

  void draw() {
 background(240);
      fill(0);
      rectMode(CENTER);
      pushMatrix();
      translate(sceneWidth/8, sceneHeight/10); 
      
      float angle = 0;

      for (int y= 2; y < sceneHeight; y = y + 40) {
        for (int x=2; x< sceneWidth; x = x + 40) {
          //float m = map(mouseX, 0, width, 0.001, 0.1);
          //float angle = noise(x * m, y * m) * 3;

          pushMatrix(); // matrix servent à créer des calques
          translate(x, y); // bouger mes nouveaux calques
          rotate(radians(angle + angleStep)); // rotation de x degres

          rect(0, 0, 35, 12);
          rect(0, 0, 12, 35);
          popMatrix();
          //deg = deg + 9;
          angle+=9;
        }

      }
      popMatrix();
//      if(anime){
  //      angleStep += 0.9;
    //  }
  }

}