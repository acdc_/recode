/*
 ------------------------
 PROJECT : PERSONAL/TEACHING
 02.02.2018

 -------------------------
 Sketch : MultiScene_RECODE_00
 Sketch Version : 0.1
 Langauge : Processing
 Version : 3.3

 * Summary : Simple tool for presenting multiple Processing
 *           sketches all in one program.
 *
 * Author :  MWebster 2018 : www.fabprojects.codes
 * Code :    https://bitbucket.org/acdc_/recode/

 -------------------------
 LIBRARIES
 - www.processing.org

 -------------------------
 USE
 1) Create a new file/new tab and paste your full sketch.
 2) Enclose your sketch within a class named, Scene+number.
     Eg. SceneOne, SceneTwo...
 3) Make sure you extend this class with "...extends Scene"
 4) Remove size() function in setup() and add :
      - student = "yourName";
      - artist 
      - title
      - sketchCode = "file name given to tab" >>> eg. sketchCode = "sketch_02.pde";
      
 5) See examples in tabs & a basic setup config in model tab:
    sketch_01.pde / sketch_02.pde / sketch_03.pde


 -------------------------
 * NOTES
 *

  -------------------------
    LICENSE

    MULTI-SCENE_01
    Copyright (C) 2018 Mark Webster
    www.fabprojects.codes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 // END OF INFO

 */