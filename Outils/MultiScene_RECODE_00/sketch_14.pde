/**
 * Sketch : Emiline Herveau
 */

class SceneFourteen extends Scene {


    void setup() {
      student = "Emiline Herveau";
      artist = "François Morellet";
      title = "Superposition de trames";
      sketchCode = "sketch_14.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/2, screenSizeH/2);
      background(0);
      strokeWeight (0.7);
      stroke (255);

      for (int x=0; x<screenSizeW; x=x+10) {
        for (int y=0; y<screenSizeH; y=y+10) {

          line(x+0, y+0, x+0, y+10 );
          line(x+0, y+0, x+100, y+0);
        }
      }


      rotate(QUARTER_PI);
      translate(-5, -500);

      for (int x=0; x<screenSizeW*2; x=x+10) {
        for (int y=-screenSizeH/2; y<screenSizeH*2; y=y+10) {

          line(x+0, y+0, x+0, y+10 );
          line(x+0, y+0, x+100, y+0);
        }
      }
      popStyle();
      popMatrix();
    }
}
