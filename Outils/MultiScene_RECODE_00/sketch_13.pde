/**
 * Sketch : Axel Durand
 */

class SceneThirteen extends Scene {

    PFont Met;
    void setup() {
      student = "Axel Durand";
      artist = "Sergio Maltagliati";
      title = "Irrwege";
      sketchCode = "sketch_13.pde";
      code = loadPde(sketchCode);

      Met = createFont("Metdemo.ttf", 32);
      textFont(Met);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/8, screenSizeH/14);
      background(255);
       translate(400, 100);
       lignes1();

       rotate(-100.0);
       translate(-360, -60);
       lignes2();

       rotate(100.0);
       translate(-100, 235);
       lignes3();
       fill(255);

       translate(100, -130, -150);
       lignes4();

       rotate(PI/4.0);
       translate(300, -530, -100);
       lignes5();

       rotate(-PI/3.0);
       translate(30, 15);
       lignes6();

       rotate(PI/5.0);
       translate(-400, 500, 100);
       lignes7();


       translate(450, -280, 100);
       rotate(PI/6.0);
       lignes8();


       translate(90, 120, -100);
       rotate(-PI/3.0);
       lignes9();

       rotate(PI/20.0);
       translate(-560, 200, 300);
       lignes10();

       translate(0, 50, 0);
       lignes11();

       translate(360, -57, 0);
       rotate(PI/20.0);
       lignes12();

       translate(0, 50);
       lignes13();


      popStyle();
      popMatrix();
    }

    void lignes1() {

  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 500, 100+i*espacey);
    fill(#000000);
    //textFont(Met);
    text("A", 100, 120);
    text("W", 130, 113); //blanche

    //crescendo / decrescendo
    text("Q", 140, 113); //noire
    text("Q", 150, 108); //noire
    text("Q", 160, 104); //noire
    text("Q", 170, 96); //noire
    text("Q", 180, 96); //noire
    text("Q", 190, 90); //noire
    text("Q", 200, 90); //noire
    text("Q", 205, 90); //noire
    text("Q", 220, 105); //noire
    text("Q", 230, 105); //noire
    text("Q", 240, 115); //noire
    text("W", 250, 118); //blanche

    text("Q", 280, 118); //noire
    text("Q", 295, 108); //noire
    text("Q", 310, 118); //noire

    text("Q", 380, 118); //la
    text("Q", 430, 95); //do
  }
}

void lignes2() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    fill(#000000);
    textSize(10);
    //textFont(Met);
    text(":", 50, 220);// clé de sol
    text("Q", 100, 208); // noire
    text("Q", 100, 225); // noire
    text("W", 160, 200); // blanche
    text("W", 300, 165); // blanche

    /* text("♪", 60, 240); //mi
     text("♪", 60, 217); // la
     text("♪", 120, 217); // la */
  }
}

void lignes3() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    textFont(Met);
    textSize(25);
    text("$", 50, 225);// clé de fa
    text("Q", 75, 205); // noire
    text("Q", 75, 195); // noire
    text("Q", 75, 220); // noire
    text("W", 140, 190); // blanche
    text("Q", 150, 205); // noire
  }
}

void lignes4() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(100, 120+i*espacey, 650, 10+i*espacey);
    text("Q", 50, 200); // si noire
  }
}

void lignes5() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(100, 100+i*espacey, 300, 10+i*espacey);
    text("Q", 0, 0); // noire
  }
}

void lignes6() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    text("Q", 0, 200); // si noire NE MARCHE PAS
  }
}

void lignes7() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 500, 100+i*espacey);
    fill(#000000);
    textFont(Met);
    text("A", 100, 130);
  }
}

void lignes8() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 450, 100+i*espacey);
    text("Q", 100, 205); // noire sol
    text("Q", 125, 195); // noire
    text("Q", 150, 210); // noire
    text("Q", 170, 195); // noire
    text("Q", 185, 175); // noire
  }
}

void lignes9() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 300, 100+i*espacey);
    text("W", 100, 125); // fa blanche
    text("W", 230, 100); // fa blanche
  }
}

void lignes10() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 400, 100+i*espacey);
    //textFont(Met);
    textSize(15);
    text(":", 50, 120);// clé de sol
    text("Q", 70, 130); // do noire
    text("Q", 70, 125); // mi noire
  }
}

void lignes11() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 400, 100+i*espacey);
    //textFont(Met);
    textSize(15);
    text("$", 50, 120);// clé de fa
    text("Q", 90, 107); // si noire
    text("Q", 120, 95); // noire
    text("Q", 160, 95); // noire
  }
}

void lignes12() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 450, 100+i*espacey);
    text("Q", 50, 105); // noire
  }
}

void lignes13() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 200, 100+i*espacey);
    text("Q", 100, 95); // noire
  }
}
}
