/**
 * Sketch : DucThuan Nguyen
 */

class SceneTwentyOne extends Scene {

    float x, y;

    void setup() {
      student = "DucThuan Nguyen";
      artist = "....";
      title = ".....";
      sketchCode = "sketch_21.pde";
      code = loadPde(sketchCode);
        x=100;

    }

    void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/2, screenSizeH/2);
      background (230, 230, 230);
        noStroke();
        int count=round(frameCount*0.05%6+1);
        //le nombre count est le chiffre pour determiner combien repetition
        //de chaque ligne car ca va etre changer selon le framerate
        y=0;
        for (int i=1; i<=count; i++) {
          float size1=50*(frameCount*0.1%7)+10;
          float size2=50*sin(frameCount*0.04)+mouseX;
          bloc1(0, y, size1, height/count/2, width/size1+1);
          //dessiner tous les blocs type 1
          bloc2(0, y+height/count/2, size2, height/count/2, width/size2+1);
          //dessiner tous les blocs type 2
          y=y+height/count;
        }

        border((mouseY/3)*sin(frameCount*0.05));

      popStyle();
      popMatrix();
    }

          void bloc1(float _x, float _y, float _xs, float _ys, float _count) {
        //prendre entrer dans la fonction la position ou se dessine un bloc, le largeur de chaue bloc et combien fois
        float j=_x;
        for (int i=1; i<=_count; i++) {
          fill(0);
          rect (j, _y, round(_xs/50*7), _ys);
          fill(#FAB1B1);
          rect (j+round(_xs/50*7), _y, round(_xs/50*12), _ys);
          fill(#7FC66C);
          rect (j+round(_xs/50*19), _y, round(_xs/50*7), round(_ys/15*8));
          rect (j+round(_xs/50*32), _y, round(_xs/50*12), _ys);
          j=j+_xs;
        }
      }

      void bloc2(float _x, float _y, float _xs, float _ys, float _count) {
        // same pour type 2
        float j=_x;
        for (int i=1; i<=_count; i++) {
          fill(0);
          rect (j+round(_xs/50*36), _y, round(_xs/50*7), _ys);
          fill(#FAB1B1);
          rect (j, _y, round(_xs/50*7), _ys);
          fill(#7FC66C);
          rect (j+round(_xs/50*7), _y, round(_xs/50*10), round(_ys/150*75));
          rect (j+round(_xs/50*17), _y, round(_xs/50*9), _ys);
          fill(#FAB1B1);
          rect (j+round(_xs/50*43), _y, round(_xs/50*7), _ys);
          j=j+_xs;
        }
      }

      void border(float _thick) {
        noStroke();
        fill(255);
        rect(0, 0, width, _thick);
        rect(0, height-_thick, width, _thick);
        rect(0, 0, _thick, height);
        rect(width-_thick, 0, _thick, height);
}

}
