/**
 * Sketch : Anaïs Gillet
 */

class SceneSeventeen extends Scene {


    void setup() {
      student = "Anaïs Gillet";
      artist = "?????";
      title = "????";
      sketchCode = "sketch_17.pde";
      code = loadPde(sketchCode);

    }

    void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/8, screenSizeH/12);
      background(255);
      fill(0);

       for (int y=0; y<=screenSizeH; y=y+10) {

         rect(0, y, screenSizeW, 5);
       }

       fill(0);
       strokeWeight(5);
       fill(255);
       triangle(screenSizeW/2+300/2, screenSizeH/2-300/2, screenSizeW/2+300/2, screenSizeH/2+300/2, screenSizeW/2-300/2, screenSizeH/2+300/2);

       noFill();
       for (int y=screenSizeW/2-300/2; y<=screenSizeW/2+300/2; y=y+10) {
         triangle(screenSizeW/2-300/2, screenSizeH/2+300/2, y, screenSizeH/2+300/2, y, screenSizeH/2-y+screenSizeW/2);
         line(2, 0, 2, screenSizeH);
         line(width-3, 0, screenSizeW-3, screenSizeH);
       }
       popStyle();
      popMatrix();

    }
}
