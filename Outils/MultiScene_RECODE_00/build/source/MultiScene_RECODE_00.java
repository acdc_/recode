import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.video.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class MultiScene_RECODE_00 extends PApplet {

/*
 * :::::::::::::::::
 * Multi-Scene Tool
 * :::::::::::::::::
 *
 * Sketch : MultiScene_RECODE_00
 * Git    : https://bitbucket.org/acdc_/recode/
 * Info   : Please read info tab
 */

// NOTES : 06.02.18: TO_DO
// * - Clean up
// *

ArrayList <Scene> theScenes;
Scene currentScene = null;
int currentSceneIndex = 17;

int screenSizeW = 1280;
int screenSizeH = 760;

boolean showSettings = false;
boolean showCode = false;
boolean autoMode = false;

Timer myTimer;
Timer animeTimer;
Movie movie;


// Global settings for sketch : size
public void settings() {
  size(screenSizeW, screenSizeH, P3D);
  smooth(4);
}

public void setup() {
  background(0, 0, 33);
  myTimer = new Timer(20000); // 20 second timer
  //animeTimer = new Timer(10000);
  noCursor();
  movie = new Movie(this, "ich_3.mp4");
  movie.loop();
  initScenes();
  if(autoMode){
    generateRandomSequence();
    resetAll();
  }else {
    setupScenes();
  }
}


public void draw() {
  background(0, 0, 33);
  if (currentScene !=null) {
    if(currentSceneIndex == 21){
      movie.play();
    }else {
      movie.stop();
    }
    currentScene.draw();
    currentScene.showInfo();
}

  ///////////////////////////// > sequenced / key actions
  if (myTimer.sequence(3000, 5000)) {
    currentScene.showCode();
  }

  if(myTimer.sequence(5000, 5000)) {
    currentScene.anime = true;
  }else {
    currentScene.anime = false;
  }

  if(showSettings){
    currentScene.showSettings();
  }
  if(showCode){
    currentScene.showCode();
  }

  if ((myTimer.finished())&&(autoMode)) {
   resetAll();
 }
}


// add each sketch to the list
public void initScenes() {
  //theScenes.clear();
  theScenes = new ArrayList<Scene>();
  theScenes.add( new SceneOne() );
  theScenes.add( new SceneTwo() );
  theScenes.add( new SceneThree() );
  theScenes.add( new SceneFour() );
  theScenes.add( new SceneFive() );
  theScenes.add( new SceneSix() );
  theScenes.add( new SceneSeven() );
  theScenes.add( new SceneEight() );
  theScenes.add( new SceneNine() );
  theScenes.add( new SceneTen() );
  theScenes.add( new SceneEleven() );
  theScenes.add( new SceneTwelve() );
  theScenes.add( new SceneThirteen() );
  theScenes.add( new SceneFourteen() );
  theScenes.add( new SceneFifteen() );
  theScenes.add( new SceneSixteen() );
  theScenes.add( new SceneSeventeen() );
  theScenes.add( new SceneEighteen() );
  theScenes.add( new SceneNineteen() );
  theScenes.add( new SceneTwenty() );
  theScenes.add( new SceneTwentyOne() );
  theScenes.add( new SceneTwentyTwo() );
}


public void movieEvent(Movie movie) {
  movie.read();
}
/*
 * Abstract Scene Class
 *
 */


abstract class Scene {

  String student = " ";
  String artist = " ";
  String title = " ";
  String sketchCode, code;
  int sceneWidth;
  int sceneHeight;
  PFont infoFont, systemFont, codeFont, titleFont;
  boolean anime = false;

  public Scene(){
    infoFont = createFont("Iosevka-Heavy", 30);
    systemFont = createFont("Iosevka-light", 15);
    codeFont = createFont("Iosevka-light", 12);
    titleFont = createFont("Iosevka-medium-oblique", 24);
  }


  public abstract void setup();
  public abstract void draw();
  //abstract void mousePressed();
  //abstract void keyPressed();

  /*
    * Set scene dimensions depending on gloable screen size settings
   */
  public void setSceneDimensions() {
    this.sceneWidth = screenSizeW;
    this.sceneHeight = screenSizeH;
  }

  public int getSceneWidth() {
    return sceneWidth;
  }

  public int getSceneHeight() {
    return sceneHeight;
  }


  /*
   * Display sketch settings
   */
  public void showSettings() {
    pushStyle();
    rectMode(CORNER);
    noStroke();
    fill(0);
    rect(25,0, 240, 283);
    String out = "";
    textFont(systemFont, 15);
    textSize(15);
    textAlign(LEFT);
    out += "--------------------------\n";
    out += "fps: " + nf(frameRate, 0, 1) + "\n";
    //out += "current animation: " + author + "\n";
    out += "current animation: " + currentSceneIndex + "\n";
    out += "auto mode on/off: " + autoMode + "\n";

    out += "Keys: \n";
    out += "arrow right - next scene \n";
    out += "arrow left - last scene \n";
    out += "d - show code \n";
    out += "a - activate auto mode \n";
    out += "i - show info \n";

    out += "--------------------------\n";
    pushMatrix();
    translate(45, 20);
    fill(255, 0, 0);
    text(out, 0, 0);
    popMatrix();
    popStyle();
  }

  /*
   * Display info for current animation
   *
   */
  public void showInfo() {
    pushStyle();
    textFont(infoFont, 26);
    textSize(30);
    //textLeading(18);
    pushMatrix();
    translate(45, height - 90);
    fill(200, 255, 0);
    text(artist, 0, 0);
    popStyle();

    pushStyle();
    textFont(titleFont, 20);
    textSize(24);
    fill(200, 255, 0);
    text(title, 0, 28);
    popStyle();

    pushStyle();
    textFont(systemFont, 14);
    textSize(16);
    fill(0, 200, 255);
    text("cod\u00e9 par "+student, 0, 60);
    popStyle();
    popMatrix();

  }

  /*
   * Display code for current sketch
   *
   */

  public void showCode(){
    textFont(codeFont);
    fill(255);
    pushMatrix();
    translate(15, -50);
    text(code,0,0);
    popMatrix();
  }

  // Function to load String for the sketch and return this String value
  public String loadPde(String _sketchName) {
    String pde = "";
    /*--------------------------------------------------------*/
    // sketchPath gets the path to the Processing sketch folder
    // [dataPath() gets the path to the data folder]
    /*--------------------------------------------------------*/
    String[] lines = loadStrings(sketchPath(_sketchName));
    for (int i=0; i<lines.length; i++) {
      // here we add each new String from lines to our pde String
      pde+=lines[i]+"\n";
    }
    return pde;
  }
}
/*
 * Timer class
 * Original class written by David Bouchard
 * http://www.deadpixel.ca
 *
 * Modified by MW_01142016
 */

class Timer {
  long startTime;
  int timeout; // in milliseconds

  Timer(int timeout) {
    this.timeout = timeout;
    this.startTime = millis();
  }

  // Returns the time elapsed since the timer started (in millis)
  public long elapsedMillis() {
    return millis() - startTime;
  }

  // Returns the time elapsed since the timer started (in seconds)
  public long elapsedSeconds() {
    return (millis() - startTime)/1000;
  }

  ////////////////////////////////////////// SEQUENCED

  /* Return true if the timer reaches a certain time
   * Also returns false after a certain duration of time
   *
   * @param : t = start time / d = duration of sequence
   */
  public boolean sequence(int t, int d) {
    long duration = t + d;
    boolean result = false;

    if (elapsedMillis() > t) {
      result = true;
    } else {
      result = false;
    }

    if (elapsedMillis() > duration) {
      result = false;
    }
    return result;
  }



  // Return true if the timer is finished
  public boolean finished() {
    if (elapsedMillis() > timeout) return true;
    return false;
  }

  // Reset the timer
  public void reset() {
    this.startTime = millis();
  }

  // Reset the timer and provide a new timeout
  // Note: we can have two functions with the same name within a class
  // as long as they have different parameters...
  public void reset(int newTimeout) {
    this.reset();  // call reset() without any parameter
    timeout = newTimeout;  // update the timeout
  }
}
/**
 * Various functions
 */

   // variables for generateRandomSequence function.
   int currentRandScene;
   int sceneNum;
   int[] randScene;

/**
 * Init scenes : size & other setup configs
 */

 public void setupScenes(){
   currentScene = theScenes.get(currentSceneIndex);
   println("Current scene chosen : "+currentSceneIndex);
   for(Scene s : theScenes){
     s.setSceneDimensions();
     s.setup();
   }
 }

 public void clearScene(){
   background(0,0,33);
 }

 public void keyPressed(){
   // get next scene
   if(keyCode == RIGHT){
     if (currentSceneIndex <theScenes.size()-1) {
       currentSceneIndex++;
       currentScene = theScenes.get( theScenes.indexOf(currentScene)+1 );
     }
     //update timer
     myTimer.reset();
   }
   // get prev scene
   if(keyCode == LEFT){
     if (currentSceneIndex > 0) {
       currentSceneIndex--;
       currentScene = theScenes.get(
         theScenes.indexOf(currentScene)-1 );
     }
     //update timer
     myTimer.reset();
   }
   if(key == 'a'){
     autoMode = !autoMode;
     setup();
   }
   if(key == 'i'){
    showSettings = !showSettings;
   }
   if(key == 'd'){
    showCode = !showCode;
   }
 }

// GENERATES A NEW RANDOM SEQUENCE FOR ANIMATION PLAY.
// EACH SEQUENCE IS UNIQUE & NEVER REPEATS AN ANIMATION ;\u2013)
// SEE checkRandom() FUNCTION BELOW FOR IMPLEMENTING THIS.
public void generateRandomSequence() {
  currentRandScene = 0;
  int scenesMax = theScenes.size();
  sceneNum = scenesMax;
  println("Total Scenes = "+sceneNum); // DEBUG
  randScene = new int[ sceneNum -1 ];

  int index = 0;
  while (index<randScene.length) {
    int num = (int)random(sceneNum);
    if (checkRandom(num)) {
      randScene[index] = num;
      index++;
    }
  }
  println(" \n/////////////////////////////////////////////////////////////////");
  for (int i = 0; i < randScene.length; ++i) {
    println("Random Scene Sequence ="+randScene[i]);
  }
   println(" \n/////////////////////////////////////////////////////////////////");
}
////////////////////////////////////////////////////////////////////
public void resetAll() {
  background(0); // clear screen
  //displayScene = false;
  myTimer.reset();
  myTimer = new Timer(20000); // 20 second timer

  currentSceneIndex = randScene[ currentRandScene ];

  currentScene = theScenes.get(currentSceneIndex);
  println("Current scene chosen : "+currentSceneIndex);

  // setup Scenes
  for (Scene theScene : theScenes) {
    theScene.setSceneDimensions();
    theScene.setup();
  }

  // CHECK TO SEE IF CURRENT RANDOM SEQUENCE HAS FINISHED. IF YES, GENERATE ANOTHER NEW SEQUENCE
  if (currentRandScene<randScene.length-1) {
    currentRandScene++;
  } else {
    generateRandomSequence();
  }
}

/**
 * Method for checking same int numbers
 * @param   num   the int number to check
 * @return        returns true or false
 */
public boolean checkRandom(int num) {
  for (int i=0; i<randScene.length; i++) {
    if (num == randScene[i]) return false;
    if (num == 0) return false; // never choose zero (could be handy !)
  }
  return true;
}
/*
 ------------------------
 PROJECT : PERSONAL/TEACHING
 02.02.2018

 -------------------------
 Sketch : MultiScene_RECODE_00
 Sketch Version : 0.1
 Langauge : Processing
 Version : 3.3

 * Summary : Simple tool for presenting multiple Processing
 *           sketches all in one program.
 *
 * Author :  MWebster 2018 : www.fabprojects.codes
 * Code :    https://bitbucket.org/acdc_/recode/

 -------------------------
 LIBRARIES
 - www.processing.org

 -------------------------
 USE
 1) Create a new file/new tab and paste your full sketch.
 2) Enclose your sketch within a class named, Scene+number.
     Eg. SceneOne, SceneTwo...
 3) Make sure you extend this class with "...extends Scene"
 4) Remove size() function in setup() and add :
      - student = "yourName";
      - artist 
      - title
      - sketchCode = "file name given to tab" >>> eg. sketchCode = "sketch_02.pde";
      
 5) See examples in tabs & a basic setup config in model tab:
    sketch_01.pde / sketch_02.pde / sketch_03.pde


 -------------------------
 * NOTES
 *

  -------------------------
    LICENSE

    MULTI-SCENE_01
    Copyright (C) 2018 Mark Webster
    www.fabprojects.codes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 // END OF INFO

 */
/**
 * Sketch : Empty Scene : Model
 */

class SceneZero extends Scene {


    public void setup() {
      student = "Emiline Herveau";
      artist = "Piet Mondrian";
      title = "New York Boogie";
      sketchCode = "sketch_01.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/2, screenSizeH/2);

      // . . . your code here

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Robin Ansart
 */

 class SceneOne extends Scene {

   float x, y, a, b, e, f, h, k, l, m, n, o;
   int r, v, g;
   int SEED;
   float taille = 30;
   int timer = 0;


 public void setup () {
   a=0;
   b=0;
   background(0);
   SEED = (int)random(1000);
   student = "Robin Ansart";
   artist = "Vera Molnar";
   title = "????????";
   sketchCode = "sketch_01.pde";
   code = loadPde(sketchCode);
 }


 public void draw() {
    rectMode(CENTER);
    noFill();
     pushMatrix();
     translate(sceneWidth/3.5f, sceneHeight/10);
     scale(0.85f);
     background(0,0,33);
     fill(0, 7);
     noStroke();
     rect(0,0,sceneWidth, sceneHeight);
     strokeWeight(1.7f); //\u00e9paisseur du trait
     randomSeed(SEED);

     for (int j =0; j<36; j=j+1) { //boucle pour les 36 carr\u00e9s
       x=20; //valeur du x \u00e0 la base
       y=120; //valeur du y \u00e0 la base

       r=PApplet.parseInt(random(3, 255)); // int est l\u00e0 pour arrondir parce que impossible d'avoir une valeur d\u00e9cimale
       v=PApplet.parseInt(random(3, 233));
       g=PApplet.parseInt(random(3, 255));


       stroke(r, v, g); // couleur du contour


       pushMatrix(); //d\u00e9but du calque
       translate(a, b);

       for ( int i = 0; i<10; i=i+1 ) { //lancement de la boucle, tourne 10x
         x=x+4; //le carr\u00e9 du milieu sera 4 px + \u00e0 droite
         y=y-4; //le carr\u00e9 du milieu sera 4 px + bas
         quad (x+e, x+f, x+h, y+k, y+l, y+m, y+n, x+o); //cr\u00e9e les points du carr\u00e9


         e=random(-taille, taille);
         f=random(-taille, taille);
         h=random(-taille, taille);
         k=random(-taille, taille);
         l=random(-taille, taille);
         m=random(-taille, taille);
         n=random(-taille, taille);
         o=random(-taille, taille);

         //println(e);
       }
       popMatrix(); //fin du calque


       a=a+120;

       if (a>600) //retour \u00e0 la ligne des carr\u00e9s
       {
         b=b+120;
         a= 0;
       }

       if (b > 600 ) {
         b=0;
       }
     }
     timer++;
     if(timer>=100){
      timer = 0;
       SEED = (int)random(1000);
     }
     popMatrix();
   }
 }
/**
 * Sketch : Cl\u00e9mence Dumoulin
 *
 */

class SceneTwo extends Scene {

  public void setup() {
    student = "Cl\u00e9mence Dumoulin";
    artist = "Victor Vasarely";
    title = "Keiho C1";
    sketchCode = "sketch_02.pde";
    code = loadPde(sketchCode);
}

/////////////////////////// DRAW ////////////////////////////

 public void draw() {
    pushStyle();
    rectMode(CORNER);
    noStroke();
    background(0,0,33);

    pushMatrix();
    translate(sceneWidth/3, sceneHeight/8);
    fill(255,0,0);
    rect(-20, -20, 580,600);

    fill(112, 34, 142);
    rect(423, 60, 82, 80);

    fill(125, 23, 25);
    rect(363, 200, 101, 101);

    fill(255, 67, 151);
    rect(363, 300, 121, 118);

    fill(255, 67, 151);
    rect(235, 0, 150, 155);

    fill(125, 23, 25);
    rect(0, 80, 115, 137);

    fill(125, 23, 25);
    rect(75, 0, 40, 85);

    fill(125, 23, 25);
    rect(75, 0, 310, 20);

    fill(125, 23, 25);
    rect(75, 20, 160, 20);

    fill(112, 34, 142);
    rect(23, 120, 90, 97);

    fill(255, 67, 151);
    rect(0, 340, 90, 198);

    fill(255, 67, 151);
    rect(90, 400, 82, 158);

    fill(255, 67, 151);
    rect(117, 480, 245, 65);

    fill(125, 23, 25);
    rect(153, 400, 110, 118);



    // rectangle rouge milieu en bas
    fill(255, 0, 0);
    rect(15, 417, 100, 100);

    fill(67, 85, 20);
    maForme(0, 0, 550, 22);

    // bleu et rose
    fill(255, 67, 151);
    rect(255, 20, 59, 60);

    pushMatrix();
    fill(131, 208, 245);
    rotate(radians(90));
    translate(-235, -332);
    maForme(255, 23, 55, -30);
    popMatrix();


    // bleu f et rouge et bordeau

    fill(255, 0, 0);
    rect(75, 55, 160, 162);

    //b
    fill(125, 23, 25);
    rect(75, 55, 40, 65);

    //v
    fill(112, 34, 142);
    rect(75, 120, 40, 97);

    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(-191, -255);
    maForme(255, 23, 155, 30);
    popMatrix();

    // Bleu moyen
    //rouge
    fill(255, 0, 0);
    rect(253, 150, 133, 110);
    //b
    noStroke();
    fill(125, 23, 25);
    rect(365, 200, 40, 60);

    //rose


    fill(255, 67, 151);
    rect(253, 99, 133, 57);

    //rouge petit
    fill(255, 0, 0);
    rect(385, 95, 20, 105);

    fill(9, 98, 198);
    maForme(255, 100, 150, -15);
    fill(125, 23, 25);
    rect(365, 280, 98, 20);
    //rouge 1


    fill(255, 0, 0);
    rect(245, 275, 120, 125);


    fill(255, 0, 0);
    rect(243, 380, 122, 100);
    fill(255, 67, 151);
    rect(365, 300, 100, 118);

    fill(125, 23, 25);
    rect(243, 400, 20, 80);

    fill(255, 0, 0);
    rect(365, 420, 100, 60);


    pushMatrix();
    fill(0, 0, 245);
    rotate(radians(90));
    translate(220, 20);
    maForme2(55, -482, 205, -15);
    popMatrix();



    // bleu moyen
    //rose

    fill(255, 67, 151);
    rect(135, 435, 20, 80);

    fill(125, 23, 25);
    rect(153, 435, 60, 80);

    pushMatrix();
    fill(9, 98, 198);
    rotate(radians(90));
    translate(-19, -25);
    maForme(463, -187, 75, 30);
    popMatrix();


    //le dernier
    //rouge 1
    fill(255, 0, 0);
    rect(34, 235, 180, 105);

    //rouge 2


    fill(255, 0, 0);
    rect(94, 340, 120, 60);

    //rose
    fill(255, 67, 151);
    rect(34, 340, 80, 76);
    //rose 2

    fill(255, 67, 151);
    rect(114, 400, 40, 20);

    fill(125, 23, 25);
    rect(154, 400, 60, 20);
      pushMatrix();
      fill(101, 200, 245);
      maForme(35, 240, 180, -15);
      popMatrix();
      popMatrix();
      popStyle();
  }


  public void maForme(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    noStroke();
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }

  public void maForme2(float _x, float _y, int _grilleTaille, float _angles) {
    pushMatrix ();
    translate(_x, _y);
    for (int y=0; y<_grilleTaille; y+=20) {
      for (int x=0; x<_grilleTaille; x+=10) {
        pushMatrix();
        translate(x, y);
        rotate(radians(_angles));
        rect(0, 0, 4, 15);
        popMatrix();
      }
    }
    popMatrix();
  }

}
/**
 * Sketch : Emiline Herveau
 */

class SceneThree extends Scene {


  public void setup() {
    student = "Emiline Herveau";
    artist = "Franck Stella";
    title = "Mas O Menos";
    sketchCode = "sketch_03.pde";
    code = loadPde(sketchCode);
  }

  public void draw() {

    pushStyle();
    rectMode(CORNER);
    background(200);
    pushMatrix();
    translate(sceneWidth/3, sceneHeight/4);
    fill(231, 230, 225);
    noStroke();
    rect(-50, -55, 645, 450);
    strokeWeight(7.2f);
    strokeCap(ROUND);
    stroke(0xff393738);

    line(0, 26, 0, 282);
    line(560, 54, 560, 309);

    for (int x=0; x<100; x=x+100) {
      for (int y=-20; y<240; y=y+10) {
        line(x, y+49, x+102, y+0);
        line(x+99, y+0, x+216, y+69);
        line(x+214, y+69, x+351, y+69);
        line(x+349, y+69, x+461, y+129);
        line(x+459, y+129, x+561, y+75);
      }
    }
    popMatrix();
    popStyle();
  }
}
/**
 * Sketch : Cl\u00e9mence Danne
 */

class SceneFour extends Scene {

  float angleStep = 0.05f;
  float hasard = 0.85f; //d\u00e9claration de variables

  public void setup() {
    student = "Cl\u00e9mence Danne";
    artist = "Horst Bartnig";
    title = "Ohne Titel";
    sketchCode = "sketch_04.pde";
    code = loadPde(sketchCode);
  }

  public void draw() {
    pushStyle();
    pushMatrix();
    translate(sceneWidth/3.8f, sceneHeight/14);
    scale(0.80f);
    background(0xffE8E9EA);
    noFill ();
    strokeWeight (2); //\u00e9paisseur des contours 2px
    stroke (100, 100, 100); //couleur du contour (gris fonc\u00e9)
    drawDessin();
    popMatrix();
    popStyle();

  }
  public void drawDessin() {
  float angle = 0;
  float angleChange = 0;
  for (int x = 50; x<850; x=x+50) {    // boucle for, x commence plac\u00e9 \u00e0 50px, il doit arriver \u00e0 850px, on incr\u00e9mente la position de 50 pixels
    for (int y = 50; y<850; y=y+50) {  //boucle for, y commence plac\u00e9 \u00e0 50, il doit arriver \u00e0 850, on incr\u00e9mente la position de 50 pixels

      pushMatrix(); //ouverture d'un nouveau calque (\u00e0 chaque nouvelle boucle, un nouveau calque)
      translate(x, y); //changement de la position d'origine du calque (0,0) aux valeurs x et y d\u00e9termin\u00e9es par la boucle.

      //rotate(radians(hasard)); //
      rotate(radians(angle + angleChange)+hasard);

      noFill (); //pas de remplissage couleur de la forme qui va \u00eatre g\u00e9n\u00e9r\u00e9e
      smooth(); //adoucir les contours des formes g\u00e9n\u00e9r\u00e9es
      strokeWeight (2); //\u00e9paisseur des contours 2px
      stroke (100, 100, 100); //couleur du contour (gris fonc\u00e9)
      ellipse (0, 0, 40, 40); //g\u00e9n\u00e9rer une ellipse par boucle. La position (0,0) permet d'avoir le cercle positionn\u00e9 en haut \u00e0 gauche du calque g\u00e9n\u00e9r\u00e9. La position du calque changeant \u00e0 chaque nouvelle boucle, le cercle change aussi de position.
      line(0, 0, 0, 20); //g\u00e9n\u00e9rer une ligne par boucle. M\u00eame fonctionnement
      line(0, 0, -20, 0); //g\u00e9n\u00e9rer une ligne par boucle. M\u00eame fonctionnement

      popMatrix(); //fermeture du calque g\u00e9n\u00e9r\u00e9
      if (anime) {
        hasard = sin((y*2.005f)+frameCount*0.005f)*45; // \u00e0 chaque nouvelle boucle, la valeur "hasard" augmente de 0.85, les calques tournent donc de 0.85\u00b0 \u00e0 chaque nouvelle boucle.
      }else {
        hasard = 0;
      }
    }
    angleChange = x*0.23f;
  }
}
}
/**
 * Sketch : Sophie Mialon
 */

class SceneFive extends Scene {

  public void setup() {
    student = "Sophie Mialon";
    artist = "Bridget Riley";
    title = "F\u00eate";
    sketchCode = "sketch_05.pde";
    code = loadPde(sketchCode);

  }

  public void draw() {
    background(200);
    rectMode(CORNER);
    randomSeed(1);
    pushMatrix();
    translate(sceneWidth/4.7f, sceneHeight/8);
    drawQuad(500, 700);
    makeShape(760, 530);
    popMatrix();
  }
  public void makeShape(int w, int h) {
    fill(230);
    noStroke();
    rect(0, 0, w, 40);
    rect(0, 0, 50, h);
    rect(710, 0, 50, h);
    rect(0, 490, w, 40);
  }

  public void drawQuad(int docHeight, int docWidth) {
    int[] colorList = {color(237, 216, 189), color(18, 152, 196), color(237, 148, 32), color(85, 129, 57), color(232, 95, 81), color(129, 150, 203), color(246, 231, 212), color(2, 2, 2)};
    for (int y = 40; y < docHeight+20; y+=20) {
      for (int x = 38; x < docWidth; x+=38) {
        //rect(x, y, 40, 25);
        int randomColor = colorList[(int)random(colorList.length) ];
        noStroke();
        fill(randomColor);
        quad(x, y, x+38, y-20, x+38, y, x, y+24);
      }
    }
  }
}
/**
 * Sketch : Hugon_Yseult
 */

class SceneSix extends Scene {

  float OffsetX = 0;

  public void setup() {
    student = "Hugon_Yseult";
    artist = "Grant Wiggins";
    title = "The Lake A Lilac Cube";
    sketchCode = "sketch_06.pde";
    code = loadPde(sketchCode);
  }
  public void draw() {
    rectMode(CORNER);
    pushMatrix();
    pushStyle();
    translate(sceneWidth/3.7f, sceneHeight/8);
    background(0xff708186);
    fill(255);
    noStroke();
    rect(0, 258.5f, 509, 258.5f);


    for (int i = 210; i < 300; i+=10) {
      fill(0xffFF368D);
      rect(125, i, 385, 5);
    }

    for (int i = 215; i < 295; i+=10) {
      fill(0xff6FA3D8);
      rect(125, i, 385, 5);
    }



    for (int i = 230; i<400; i+=92) {
      for (int j = 75; j<400; j+=92) {

        pushMatrix();
        translate(j, i);
        for (int k = 0; k < 48; k+=1) {
          fill(255, 0, 0);
          rect(k, k, 46, 46);
          if (k == 47) {
            fill( 0xffFF00E2);
            rect(0, 0, 46, 46);
          }
        }
        popMatrix();
      }
    }

    for (int i = 276; i<400; i+=92) {
      for (int j = 122; j<400; j+=92) {

        pushMatrix();
        translate(j, i);
        for (int k = 0; k < 48; k+=1) {
          fill(255, 0, 0);
          rect(k, k, 46, 46);
          if (k == 47 ) {
            fill( 0xffFF00E2);
            rect(0, 0, 46, 46);
          }
        }
        popMatrix();
      }
    }

    for (int y = 168; y<400; y+=92) {
      fill(0xffFF00E2);
      rect(y, 322, 46, 46);
    }

    for (int y = 122; y<400; y+=92) {
      fill(255, 0, 0);
      rect(y, 322, 46, 46);
    }
    OffsetX = 0;
    for (int y = 0; y < 200; y = y + 40) {
      if(anime) {
        OffsetX = sin((y*10.0f)+frameCount*0.015f)*60;
      }else {
        OffsetX = 0;
      }
      for (int i = 125; i < 200; i= i +40) {
        pushMatrix();
        translate(i+OffsetX, 0);
        fill(0xffFF368D);
        rect(0, 10+y, 385, 5 );
        fill(0xff6FA3D8);
        rect(0, 15+y, 385, 5 );
        fill(0xffFF368D);
        rect(60, 20+y, 385, 5 );
        fill(0xff6FA3D8);
        rect(60, 25+y, 385, 5 );
        fill(0xffFF368D);
        rect(120, 30+y, 385, 5 );
        fill(0xff6FA3D8);
        rect(120, 35+y, 385, 5 );
        fill(0xffFF368D);
        rect(180, 40+y, 385, 5 );
        fill(0xff6FA3D8);
        rect(180, 45+y, 385, 5 );
        popMatrix();
      }
    }
    fill(0xff708186);
    rect(509, 0, 300, 400);
    popMatrix();
    popStyle();
  }
}
/**
 * Sketch : Emiline Herveau
 */

class SceneSeven extends Scene {
    
    float epaisseur = 0.1f;
    float phi = (sqrt(5)+1)/2;
    int SEED;
    int timer;
  
    public void setup() {
      student = "Emiline Herveau";
      artist = "Piet Mondrian";
      title = "Composition II en rouge, bleu, jaune";
      sketchCode = "sketch_07.pde";
      code = loadPde(sketchCode);
      background(255);
      SEED = (int)random(1000);
      timer = 0;

    }

    public void draw() {
      rectMode(CORNER);
      pushMatrix();
      pushStyle();
      translate(sceneWidth/8, sceneHeight/10); 
      randomSeed(SEED);
              stroke(0);
        strokeWeight(0.05f);
      translate(sceneHeight/phi, 0);
      scale(sceneHeight*0.75f);
      fill(0);
      rect(-50, -50, 600, 600);
      for (float i=0; i<12; i++) {
        int poss = 0;
        random(poss);

        int hasard = PApplet.parseInt(random(0, 4));

        int couleur [] = {color(0xffFF0329), color(0xff1956F7), color(0xffFEFF00), color(0xffFFFFFF)};
        fill(couleur[hasard]);



        rect(0, 0, 1, 1);
        epaisseur = epaisseur  - 0.005f;
        scale(1/phi);
        rotate(PI/2);
        translate(1/phi, 0);
      }
      timer++;
      if (timer>=60) {
        timer = 0;
        SEED = (int)random(1000);
      }
      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Rose-Marie Devignes
 */

class SceneEight extends Scene {
    
    int couleur [] = {color(62,128,208), color(222,255,0), color (249,141,157), color(87,225,77)};
    
    public void setup() {
      student = "Rose-Marie Devignes";
      artist = "John Armleder";
      title = "Cast Iron";
      sketchCode = "sketch_08.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
    rectMode(CORNER);
    background(0);
      pushMatrix();
      pushStyle();
      randomSeed(1);
      translate(sceneWidth/7, sceneHeight/8); 
      //scale(0.7);
      
      fill(0xffE3DCCA);
      rect(-30,-30, sceneWidth, sceneHeight);
      for ( int y=5; y<sceneHeight; y=y+90) {
        for (int x=10; x<sceneWidth-sceneWidth/1.8f;x+=100) {
          noStroke();
          fill(couleur [ PApplet.parseInt(random(0, 4))]);
          ellipse(x+50, y+70, sceneWidth/35, sceneWidth/35);
        }
      }
      
      strokeWeight(sceneWidth/40);
      for ( int x=-10; x<(sceneWidth); x=x+70) {
        for (int y=-25; y<(sceneHeight); y=y+10) {
          stroke(255, 0, 0);         
          line(x+(sceneWidth/3), y, x+(sceneWidth/3), sceneHeight);
        }
      }

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Rose-Marie Devignes
 */

class SceneNine extends Scene {


  public void setup() {
    student = "Emiline Herveau";
    artist = "Vera Molnar";
    title = "Un, Deux, Trois";
    sketchCode = "sketch_09.pde";
    code = loadPde(sketchCode);
  }

  public void draw() {
    rectMode(CORNER);
    pushMatrix();
    pushStyle();
    randomSeed(1); // ADD the seed somewhere > animations !
    translate(90, 125);
    scale(0.73f);
    background(255, 255, 255);
    fill(0);
    stroke(0);
    strokeWeight(2);

    for (int y=0; y<sceneHeight/3; y+=40) {
      for (int x=0; x<sceneWidth; x+=40) {
        float angleRandom=random(-75, 75);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        popMatrix();
      }
    }

    for (int y=sceneHeight/3; y<sceneHeight-370; y+=40) {
      for (int x=0; x<sceneWidth; x+=50) {
        float angleRandom=random(-75, 75);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        rect(5, 20, 40, 10, 200);
        popMatrix();
      }
    }
    for (int y=sceneHeight-350; y<sceneHeight; y+=40
      ) {
      for (int x=0; x<sceneWidth; x+=40) {
        float angleRandom=random(-180, 180);
        pushMatrix();
        translate(x, y);
        rotate (radians(angleRandom));
        rect(0, -10, 40, 10, 200);
        rect(5, 5, 40, 10, 200);
        rect(10, 20, 40, 10, 200);
        popMatrix();
      }
    }
    noFill();
    strokeWeight( 50) ;
    stroke(255);
    rect(-60, -60, sceneWidth+100, sceneHeight+100);

    popStyle();
    popMatrix();
  }
}
/**
 * Sketch : Empty Scene : Model
 */

class SceneTen extends Scene {

  float x1;
  float y1;
  float x2;
  float y2;
  float angleStep;


  public void setup() {
    student = "Elisabeth Musielak";
    artist = "Hans Kuiper";
    title = "???????";
    sketchCode = "sketch_10.pde";
    code = loadPde(sketchCode);
  }

  public void draw() {
 background(240);
      fill(0);
      rectMode(CENTER);
      pushMatrix();
      translate(sceneWidth/8, sceneHeight/10); 
      
      float angle = 0;

      for (int y= 2; y < sceneHeight; y = y + 40) {
        for (int x=2; x< sceneWidth; x = x + 40) {
          //float m = map(mouseX, 0, width, 0.001, 0.1);
          //float angle = noise(x * m, y * m) * 3;

          pushMatrix(); // matrix servent \u00e0 cr\u00e9er des calques
          translate(x, y); // bouger mes nouveaux calques
          rotate(radians(angle + angleStep)); // rotation de x degres

          rect(0, 0, 35, 12);
          rect(0, 0, 12, 35);
          popMatrix();
          //deg = deg + 9;
          angle+=9;
        }

      }
      popMatrix();
//      if(anime){
  //      angleStep += 0.9;
    //  }
  }

}
/**
 * Sketch : Cl\u00e9zio Descluse
 */

class SceneEleven extends Scene {

  PFont font;
  float x, y;
  char[] chars ={'\u2588', '\u2588', '\u2593', '\u2592', '\u2591', '#', '\u2261', '%', '$', '@', '&'};
  char c;
  int index;
  int marge = 30;
  int seed;

  public void setup() {
    student = "Cl\u00e9zio Descluse";
    artist = "Jodi";
    title = "New York Boogie";
    sketchCode = "sketch_11.pde";
    code = loadPde(sketchCode);
    font = createFont("font.ttf", 50);
    seed = (int)random(1000);
  }

  public void draw() {
    background(0);
    pushStyle();
    rectMode(CENTER);
    textFont(font);
    textAlign(CENTER, CENTER);
    randomSeed(seed);
    pushMatrix();
    translate(sceneWidth/7, sceneHeight/9);
    textFont(font);
    scale(0.80f);
    for (int y=marge; y<sceneHeight-marge; y+=30) {
      for (int x=marge; x<sceneWidth-marge; x+=30) {
        
        int dice = (int)random(8);
        if (dice<2) {
          index = (int)random(33, 127);
          c = PApplet.parseChar(index);
          fill(255);
          text(c, x, y);
        } else if (dice>2 && dice<4) {
          int theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme1(x, y, longeur, theColour);
        } else if (dice>4 && dice<6) {
          int theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme2(x, y, longeur, theColour);
        } else if (dice>6 && dice<8) {
          int theColour = color(random(100, 255), random(5, 255), random(0, 255));
          float longeur = random(5, 100);
          forme3(x, y, longeur, theColour);
          x=x-15;
        } else {
        }
      }
    }
    popMatrix();
    popStyle();
  }

  public void keyPressed() {
    seed = (int)random(0, 1000);
  }

  public void forme1(float _x, float _y, float _len, int _c) {
    noStroke();
    pushMatrix();
    translate(_x, _y);
    pushMatrix();
    translate(5, 6);
    rect(0, 0, _len, 30);
    popMatrix();
    popMatrix();
  }

  public void forme2(float _x, float _y, float _len, int _c) {
    int dice = (int)random(6);
    if (dice<3) {
      int theColour = color(random(100, 255), random(5, 255), random(0, 255));
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, _len, 30);
      popMatrix();
      popMatrix();
    }
  }



  public void forme3(float _x, float _y, float _len, int _c) {
    int dice = (int)random(6);
    if (dice<3) {
      int theColour = color(0);
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, 10, 10);
      popMatrix();
      popMatrix();
    } else {
      int theColour = color(255);
      fill(theColour);
      noStroke();
      pushMatrix();
      translate(_x, _y);
      pushMatrix();
      translate(5, 6);
      rect(0, 0, 10, 10);
      popMatrix();
      popMatrix();
    }
  }
}
/**
 * Sketch : Evan Leli\u00e8vre
 */

class SceneTwelve extends Scene {

    float deg = 0;

    public void setup() {
      student = "Evan Leli\u00e8vre";
      artist = "Julio Le Parc";
      title = "Modulation 1159";
      sketchCode = "sketch_12.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/4, screenSizeH/6);
      fill(255);
      noStroke();
      background(0);

      float opacite = 160;
      for(int i = 0; i < 6; i++) {
        pushMatrix();
        translate(390,230);
        rotate(radians(deg));
        fill(200, opacite);
        rect(60,0,120,120);

        deg = deg+18;
        popMatrix();
      }

      deg = 253;
      for(int i = 0; i < 5; i++) {
       pushMatrix();
       translate(270,475);
       rotate(radians(deg));
       fill(200, opacite);
       rect(60,0,120,120);
       popMatrix();
       deg = deg-18;
      }

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Axel Durand
 */

class SceneThirteen extends Scene {

    PFont Met;
    public void setup() {
      student = "Axel Durand";
      artist = "Sergio Maltagliati";
      title = "Irrwege";
      sketchCode = "sketch_13.pde";
      code = loadPde(sketchCode);

      Met = createFont("Metdemo.ttf", 32);
      textFont(Met);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/8, screenSizeH/14);
      background(255);
       translate(400, 100);
       lignes1();

       rotate(-100.0f);
       translate(-360, -60);
       lignes2();

       rotate(100.0f);
       translate(-100, 235);
       lignes3();
       fill(255);

       translate(100, -130, -150);
       lignes4();

       rotate(PI/4.0f);
       translate(300, -530, -100);
       lignes5();

       rotate(-PI/3.0f);
       translate(30, 15);
       lignes6();

       rotate(PI/5.0f);
       translate(-400, 500, 100);
       lignes7();


       translate(450, -280, 100);
       rotate(PI/6.0f);
       lignes8();


       translate(90, 120, -100);
       rotate(-PI/3.0f);
       lignes9();

       rotate(PI/20.0f);
       translate(-560, 200, 300);
       lignes10();

       translate(0, 50, 0);
       lignes11();

       translate(360, -57, 0);
       rotate(PI/20.0f);
       lignes12();

       translate(0, 50);
       lignes13();


      popStyle();
      popMatrix();
    }

    public void lignes1() {

  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 500, 100+i*espacey);
    fill(0xff000000);
    //textFont(Met);
    text("A", 100, 120);
    text("W", 130, 113); //blanche

    //crescendo / decrescendo
    text("Q", 140, 113); //noire
    text("Q", 150, 108); //noire
    text("Q", 160, 104); //noire
    text("Q", 170, 96); //noire
    text("Q", 180, 96); //noire
    text("Q", 190, 90); //noire
    text("Q", 200, 90); //noire
    text("Q", 205, 90); //noire
    text("Q", 220, 105); //noire
    text("Q", 230, 105); //noire
    text("Q", 240, 115); //noire
    text("W", 250, 118); //blanche

    text("Q", 280, 118); //noire
    text("Q", 295, 108); //noire
    text("Q", 310, 118); //noire

    text("Q", 380, 118); //la
    text("Q", 430, 95); //do
  }
}

public void lignes2() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    fill(0xff000000);
    textSize(10);
    //textFont(Met);
    text(":", 50, 220);// cl\u00e9 de sol
    text("Q", 100, 208); // noire
    text("Q", 100, 225); // noire
    text("W", 160, 200); // blanche
    text("W", 300, 165); // blanche

    /* text("\u266a", 60, 240); //mi
     text("\u266a", 60, 217); // la
     text("\u266a", 120, 217); // la */
  }
}

public void lignes3() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    textFont(Met);
    textSize(25);
    text("$", 50, 225);// cl\u00e9 de fa
    text("Q", 75, 205); // noire
    text("Q", 75, 195); // noire
    text("Q", 75, 220); // noire
    text("W", 140, 190); // blanche
    text("Q", 150, 205); // noire
  }
}

public void lignes4() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(100, 120+i*espacey, 650, 10+i*espacey);
    text("Q", 50, 200); // si noire
  }
}

public void lignes5() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(100, 100+i*espacey, 300, 10+i*espacey);
    text("Q", 0, 0); // noire
  }
}

public void lignes6() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 500, 100+i*espacey);
    text("Q", 0, 200); // si noire NE MARCHE PAS
  }
}

public void lignes7() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 500, 100+i*espacey);
    fill(0xff000000);
    textFont(Met);
    text("A", 100, 130);
  }
}

public void lignes8() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 200+i*espacey, 450, 100+i*espacey);
    text("Q", 100, 205); // noire sol
    text("Q", 125, 195); // noire
    text("Q", 150, 210); // noire
    text("Q", 170, 195); // noire
    text("Q", 185, 175); // noire
  }
}

public void lignes9() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 300, 100+i*espacey);
    text("W", 100, 125); // fa blanche
    text("W", 230, 100); // fa blanche
  }
}

public void lignes10() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 400, 100+i*espacey);
    //textFont(Met);
    textSize(15);
    text(":", 50, 120);// cl\u00e9 de sol
    text("Q", 70, 130); // do noire
    text("Q", 70, 125); // mi noire
  }
}

public void lignes11() {
  //PFont Met;
  //Met = createFont("Metdemo.ttf", 32);
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 400, 100+i*espacey);
    //textFont(Met);
    textSize(15);
    text("$", 50, 120);// cl\u00e9 de fa
    text("Q", 90, 107); // si noire
    text("Q", 120, 95); // noire
    text("Q", 160, 95); // noire
  }
}

public void lignes12() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 450, 100+i*espacey);
    text("Q", 50, 105); // noire
  }
}

public void lignes13() {
  int espacey=(height-10)/100;
  int espacex=(width-10)/100;
  for (int i=0; i<=4; i++) {
    line(50, 100+i*espacey, 200, 100+i*espacey);
    text("Q", 100, 95); // noire
  }
}
}
/**
 * Sketch : Emiline Herveau
 */

class SceneFourteen extends Scene {


    public void setup() {
      student = "Emiline Herveau";
      artist = "Fran\u00e7ois Morellet";
      title = "Superposition de trames";
      sketchCode = "sketch_14.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/2, screenSizeH/2);
      background(0);
      strokeWeight (0.7f);
      stroke (255);

      for (int x=0; x<screenSizeW; x=x+10) {
        for (int y=0; y<screenSizeH; y=y+10) {

          line(x+0, y+0, x+0, y+10 );
          line(x+0, y+0, x+100, y+0);
        }
      }


      rotate(QUARTER_PI);
      translate(-5, -500);

      for (int x=0; x<screenSizeW*2; x=x+10) {
        for (int y=-screenSizeH/2; y<screenSizeH*2; y=y+10) {

          line(x+0, y+0, x+0, y+10 );
          line(x+0, y+0, x+100, y+0);
        }
      }
      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Michel Ludivine
 */

class SceneFifteen extends Scene {

    float taille =120;// taille des carr\u00e9 millieu
    float taille2 =300; //taille  carr\u00e9 exterieur
    float x = 150; //taille carr\u00e9 horizontal
    float y =150; // taille carr\u00e9 vertical


    public void setup() {
      student = "Michel Ludivine";
      artist = "Jean Pierre Yvaral";
      title = "Quadrature E";
      sketchCode = "sketch_15.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/5, screenSizeH/8);
      background(0);
      noFill();
      stroke(0xff050355);
      strokeWeight(4);

      rectMode(CENTER);
      // . . . your code here

      for (int i = 5; i<37; i+=4) {
        translate(-10.8f, 14.4f); // d\u00e9placer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        rect(177, 1, taille, taille);
        taille = taille -4; // taille de chaque carr\u00e9-2
      }

      float taille3 =120;// taille des carr\u00e9 millieu
      float taille4 =300; //taille  carr\u00e9 exterieur
      float a = 537; //taille carr\u00e9 horizontal
      float b =35; // taille carr\u00e9 vertical

      noFill();
      stroke(0xff050355);
      strokeWeight(4);

      //rectMode(CENTER); // carr\u00e9 exterieur centr\u00e9


      for (int i = 1; i<30; i+=3) {
        rect(a, b, taille4 ,taille4);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        a = a -3;
        b= b -3;
       taille4 = taille4 -23; // taille de chaque carr\u00e9-20
      }

      //rectMode(CORNER); // carr\u00e9 exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(-10.8f, -11); // d\u00e9placer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        rect(564, 63, taille3, taille3);
        taille3 = taille3 -4; // taille de chaque carr\u00e9-2
      }

      float taille5 =120;// taille des carr\u00e9 millieu
      float taille6 =300; //taille  carr\u00e9 exterieur
      float c = 623; //taille carr\u00e9 horizontal
      float d =424; // taille carr\u00e9 vertical

      noFill();
      stroke(0xff050355);
      strokeWeight(4);

      //rectMode(CENTER); // carr\u00e9 exterieur centr\u00e9


      for (int i = 1; i<30; i+=3) {
        rect(c, d, taille6 ,taille6);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        c = c +3;
        d= d -3;
       taille6 = taille6 -23; // taille de chaque carr\u00e9-20
      }

    //  rectMode(CORNER); // carr\u00e9 exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(+14.5f, -10.5f); // d\u00e9placer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        rect(473, 450, taille5, taille5);
        taille5 = taille5 -4; // taille de chaque carr\u00e9-2
      }

      float taille7 =120;// taille des carr\u00e9 millieu
      float taille8 =300; //taille  carr\u00e9 exterieur
      float e = 207; //taille carr\u00e9 horizontal
      float f =508; // taille carr\u00e9 vertical

      noFill();
      stroke(0xff050355);
      strokeWeight(4);

      //rectMode(CENTER); // carr\u00e9 exterieur centr\u00e9


      for (int i = 1; i<30; i+=3) {
        rect(e, f, taille8 ,taille8);
         float degrade = map (i, 26, 0, 0, 255);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        e = e +3;
        f= f +3;
       taille8 = taille8 -23; // taille de chaque carr\u00e9-20
      }

    //  rectMode(CORNER); // carr\u00e9 exterieur dans le coin


      for (int i = 5; i<37; i+=4) {
        translate(+14.5f, +13.9f); // d\u00e9placer calque
        noFill();
        float degrade = map (i, 25, 0, 0, 900);
        stroke(0, 0, degrade); // d\u00e9grad\u00e9 bleu
        strokeWeight(4);
        rect(57, 360, taille7, taille7);
        taille7 = taille7 -4; // taille de chaque carr\u00e9-2
}

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Younes Boutarhroucht
 */

class SceneSixteen extends Scene {


    public void setup() {
      student = "Younes Boutarhroucht";
      artist = "Kasimir Malevitch";
      title = "Suprematist Compositions";
      sketchCode = "sketch_16.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/4, screenSizeH/12);
      background(0xffC4C0C0);



      pushMatrix();
      stroke(0xff1C4FE5);
      strokeWeight(4);
      translate(500,-10);
      rotate(PI/6.0f);
      line(158,80,190,580);
      popMatrix();



      //yellow line
      pushMatrix();
      stroke(0xffFFC012);
      strokeWeight(4);
      translate(557,-5);
      rotate(PI/4.0f);
      line(230,170,190,355);
      popMatrix();




      pushMatrix();
      fill(0xffF51616);
      noStroke();
      translate(375,-370);
      rotate(PI/3.0f);
      rect(503,158,65, 15);
      popMatrix();




      pushMatrix();
      noStroke();
      fill(0xffF2A70F);
      triangle(386,317, 350, 180, 426, 180);
      popMatrix();

      pushMatrix();
      stroke(0);
      strokeWeight(2);
      translate(140,0);
      line(4,175,75,175);
      popMatrix();



      pushMatrix();
      stroke(0xffFA1717);
      strokeWeight(4);
      translate(120,0);
      line(90,400,140,400);
      popMatrix();

      //red small line
      pushMatrix();
      stroke(0xffFA1717);
      strokeWeight(4);
      translate(370,195);
      rotate(PI/33.0f);
      line(16,380,59,402);
      popMatrix();

      pushMatrix();
      stroke(0xffFA1717);
      strokeWeight(4);
      translate(165,0);
      line(30,90,70,400);
      popMatrix();

      pushMatrix();
      stroke(0xff080707);
      strokeWeight(4);
      translate(150,0);
      line(280,210,295,354);
      popMatrix();



      pushMatrix();
      stroke(0);
      strokeWeight(2);
      translate(99,0);
      line(79,218,79,160);
      popMatrix();


      pushMatrix();
      fill(0xffF51616);
      noStroke();
      translate(375,0);
      rotate(radians(-10));
      rect(11,58,80, 15);
      popMatrix();



      fill(0);
      rectMode(CENTER);
      pushMatrix();
      noStroke();
      fill(0xffF51919);
      translate(129,0);
      triangle(100,150, 100, 180, 300, 90);
      triangle(100,180,300,80,300,180);
      popMatrix();

      pushMatrix();
      fill(0xffF51616);
      noStroke();
      translate(90,0);
      rotate(radians(-30));
      rect(85,360,450, 15);
      popMatrix();



      pushMatrix();
      noStroke();
      fill(0xff050404);
      translate(360,0);
      rotate(radians(-10));
      rect(0,100, 150, 50);
      popMatrix();

      pushMatrix();
      translate(360,0);
      rotate(radians(-10));
      rect(1,60, 50, 50);
      popMatrix();




      pushMatrix();
      fill(0xff080707);
      strokeWeight(2);
      translate(365,0);
      rotate(radians(-10));
      rect(1,290,60, 14);
      popMatrix();



      //black line
      pushMatrix();
      stroke(0);
      strokeWeight(4);
      translate(650,-1);
      rotate(PI/4.0f);
      line(45,400,215,300);
      popMatrix();




      //small quad blue
      pushMatrix();
      fill(0xff2E487E);
      noStroke();
      translate(350,466);
      rotate(PI/30.0f);
      quad(25, 11, 36, 25, 30, 30, 15, 30);
      popMatrix();



      //medium quad blue
      pushMatrix();
      fill(0xff2E487E);
      noStroke();
      translate(319,570);
      rotate(PI/30.0f);
      quad(25, 11, 46, 25, 40, 40, 15, 30);
      popMatrix();

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : Ana\u00efs Gillet
 */

class SceneSeventeen extends Scene {


    public void setup() {
      student = "Ana\u00efs Gillet";
      artist = "?????";
      title = "????";
      sketchCode = "sketch_17.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/8, screenSizeH/12);
      background(255);
      fill(0);

       for (int y=0; y<=screenSizeH; y=y+10) {

         rect(0, y, screenSizeW, 5);
       }

       fill(0);
       strokeWeight(5);
       fill(255);
       triangle(screenSizeW/2+300/2, screenSizeH/2-300/2, screenSizeW/2+300/2, screenSizeH/2+300/2, screenSizeW/2-300/2, screenSizeH/2+300/2);

       noFill();
       for (int y=screenSizeW/2-300/2; y<=screenSizeW/2+300/2; y=y+10) {
         triangle(screenSizeW/2-300/2, screenSizeH/2+300/2, y, screenSizeH/2+300/2, y, screenSizeH/2-y+screenSizeW/2);
         line(2, 0, 2, screenSizeH);
         line(width-3, 0, screenSizeW-3, screenSizeH);
       }
       popStyle();
      popMatrix();

    }
}
/**
 * Sketch : Maxence Pinchon
 */

class SceneEighteen extends Scene {

      PFont  f18;
      PImage img;
      int C;
      float gris ;
      int x;
      int y;

    public void setup() {
      student = "Maxence Pinchon";
      artist = "???? Barclay";
      title = "?????";
      sketchCode = "sketch_18.pde";
      code = loadPde(sketchCode);

            img = loadImage("lovehateb.jpg");
            f18 = createFont("CheltenhamStd-Book.otf", 20);
            textFont(f18);

    }

    public void draw() {
      pushMatrix();
      pushStyle();

      translate(screenSizeW/4, screenSizeH/8);
      background(173);
      stroke(0);
      strokeWeight(5);
      for ( x=0; x < 600; x+=8) {
        stroke(0, 0, 255);
        strokeWeight(4);
        line(x, 0, x, 600);
      }
      strokeWeight(3);
      for ( x=0; x < 600; x+=8) {
        for ( y=0; y < 600; y+=8) {

          C = img.get(x, y);

          if (C<=color(30)) {
            stroke(255, 0, 0);
            line(x, y-3.5f, x, y+3.5f);
          }
        }
    }

      popStyle();
      popMatrix();
  }
}
/**
 * Sketch : Hugo Molines
 */

class SceneNineteen extends Scene {

    float x, y, taille, rotation, l, s, var;
    int r, v, b;

    public void setup() {
      student = "Hugo Molines";
      artist = "Josef Muller Brockmann";
      title = "New York Boogie";
      sketchCode = "sketch_19.pde";
      code = loadPde(sketchCode);

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/4, screenSizeH/8);
      noFill();
      stroke(5);
      background (0);
      r=PApplet.parseInt(random(0, 250));
      v=PApplet.parseInt(random(0, 250));
      b=PApplet.parseInt(random(0, 250));
      strokeWeight(s);
      s=s+1*var;

      if (s==100) {
        var=-1;
      }

      if (s==0) {
        var=1;
      }
      //float angle = 0;
      for (int i = 20; i < 800; i = i+65) {
          float r = random(0, 100);
          josef(300, 300, i, r);
          //angle += r * 0.0013;
        }

        popStyle();
        popMatrix();
      }

      public void josef(float x, float y, float taille, float rotation) {

      pushMatrix();
      translate(x, y);
      rotate(radians(rotation));
      strokeCap(ROUND);
      //stroke(r,v,b);
      stroke(0xffD8FF00);
      arc(0, 0, taille, taille, radians(0), radians(20));
      //stroke(r,v,b);
      stroke(0xffFF0318);
      arc(0, 0, taille, taille, radians(20), radians(40));
      //stroke(r,v,b);
      stroke(0xff0383FF);
      arc(0, 0, taille, taille, radians(40), radians(65));

      //stroke(r,v,b);
      stroke(0xffD8FF00);
      arc(0, 0, taille, taille, radians(90), radians(110));
      //stroke(r,v,b);
      stroke(0xffFF0318);
      arc(0, 0, taille, taille, radians(110), radians(135));
      //stroke(r,v,b);
      stroke(0xff0383FF);
      arc(0, 0, taille, taille, radians(135), radians(155));

      stroke(0xffD8FF00);
      arc(0, 0, taille, taille, radians(180), radians(200));
      stroke(0xffFF0318);
      arc(0, 0, taille, taille, radians(200), radians(223));
      stroke(0xff0383FF);
      arc(0, 0, taille, taille, radians(223), radians(245));

      stroke(0xffD8FF00);
      arc(0, 0, taille, taille, radians(270), radians(290));
      stroke(0xffFF0318);
      arc(0, 0, taille, taille, radians(290), radians(315));
      stroke(0xff0383FF);
      arc(0, 0, taille, taille, radians(315), radians(335));

      popMatrix();

    }
}
/**
 * Sketch : DucThuan Nguyen
 */

class SceneTwenty extends Scene {

      float x1 [];
      float x2 [];
      float y1 [];
      float y2 [];

    public void setup() {
      student = "DucThuan Nguyen";
      artist = "????";
      title = "??????";
      sketchCode = "sketch_20.pde";
      code = loadPde(sketchCode);
        x1 = new float [10];
         y1 = new float [10];
         x2 = new float [10];
         y2 = new float [10];
         for (int k=1; k<10; k++){
           x1[k]=random (0,600);
           x2[k]=random (0,600);
           y1[k]=random (0,600);
           y2[k]=random (0,600);
         }

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      translate(screenSizeW/6, screenSizeH/8);

      background (200,200,200);
       noStroke();
       for (int l=1; l<20; l++){
         fill (0,0,0,4);
         ellipse (screenSizeW/4,screenSizeW/6+l,315,315);
       }
       fill (210);

       ellipse (screenSizeW/4,screenSizeW/6,310,310);

       for (int j=1; j<10; j++){
         for (int i=3; i<30; i++) {
           strokeWeight(i);
           stroke (255,255,255,4);
           line (x1[j],y1[j],x2[j],y2[j]);
         }
         strokeWeight(2);
         stroke (255,255,255,255);
         line (x1[j],y1[j],x2[j],y2[j]);
       }
       for (int k=1; k<10; k++){
         x1[k]=x1[k]+random (-15,15);
         x2[k]=x2[k]+random (-15,15);
         y1[k]=y1[k]+random (-15,15);
         y2[k]=y2[k]+random (-15,15);
       }

      popStyle();
      popMatrix();
    }
}
/**
 * Sketch : DucThuan Nguyen
 */

class SceneTwentyOne extends Scene {

    float x, y;

    public void setup() {
      student = "DucThuan Nguyen";
      artist = "....";
      title = ".....";
      sketchCode = "sketch_21.pde";
      code = loadPde(sketchCode);
        x=100;

    }

    public void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/2, screenSizeH/2);
      background (230, 230, 230);
        noStroke();
        int count=round(frameCount*0.05f%6+1);
        //le nombre count est le chiffre pour determiner combien repetition
        //de chaque ligne car ca va etre changer selon le framerate
        y=0;
        for (int i=1; i<=count; i++) {
          float size1=50*(frameCount*0.1f%7)+10;
          float size2=50*sin(frameCount*0.04f)+mouseX;
          bloc1(0, y, size1, height/count/2, width/size1+1);
          //dessiner tous les blocs type 1
          bloc2(0, y+height/count/2, size2, height/count/2, width/size2+1);
          //dessiner tous les blocs type 2
          y=y+height/count;
        }

        border((mouseY/3)*sin(frameCount*0.05f));

      popStyle();
      popMatrix();
    }

          public void bloc1(float _x, float _y, float _xs, float _ys, float _count) {
        //prendre entrer dans la fonction la position ou se dessine un bloc, le largeur de chaue bloc et combien fois
        float j=_x;
        for (int i=1; i<=_count; i++) {
          fill(0);
          rect (j, _y, round(_xs/50*7), _ys);
          fill(0xffFAB1B1);
          rect (j+round(_xs/50*7), _y, round(_xs/50*12), _ys);
          fill(0xff7FC66C);
          rect (j+round(_xs/50*19), _y, round(_xs/50*7), round(_ys/15*8));
          rect (j+round(_xs/50*32), _y, round(_xs/50*12), _ys);
          j=j+_xs;
        }
      }

      public void bloc2(float _x, float _y, float _xs, float _ys, float _count) {
        // same pour type 2
        float j=_x;
        for (int i=1; i<=_count; i++) {
          fill(0);
          rect (j+round(_xs/50*36), _y, round(_xs/50*7), _ys);
          fill(0xffFAB1B1);
          rect (j, _y, round(_xs/50*7), _ys);
          fill(0xff7FC66C);
          rect (j+round(_xs/50*7), _y, round(_xs/50*10), round(_ys/150*75));
          rect (j+round(_xs/50*17), _y, round(_xs/50*9), _ys);
          fill(0xffFAB1B1);
          rect (j+round(_xs/50*43), _y, round(_xs/50*7), _ys);
          j=j+_xs;
        }
      }

      public void border(float _thick) {
        noStroke();
        fill(255);
        rect(0, 0, width, _thick);
        rect(0, height-_thick, width, _thick);
        rect(0, 0, _thick, height);
        rect(width-_thick, 0, _thick, height);
}

}
/**
 * Sketch : Meddy
 */
 

class SceneTwentyTwo extends Scene {
      PImage type1, type2, type3, type4, type5, type6;
      String signe = "X";
      int rouge;
      int clr;
      int x, y, x1, y1;
      PImage img [] ;

    public void setup() {
      student = "Meddy";
      artist = "Ken Knowlton";
      title = "Studies In Perception #1";
      sketchCode = "sketch_22.pde";
      code = loadPde(sketchCode);

      img = new PImage [14];
      for (int i =1; i < 14; i ++) {
        img[i] = loadImage( i +".png");
        // img[i].resize(7, 0);
      }
    }

    public void draw() {
      pushMatrix();
      pushStyle();
      //translate(screenSizeW/2, screenSizeH/2);

      drawMovie();

      popStyle();
      popMatrix();
    }

    public void drawMovie() {
          movie.loadPixels();
          for (int x=1; x<=width; x+=7) {
            for (int y=1; y<=height; y+=7) {

        clr=movie.get(x, y);
        fill(clr);

        float value = brightness(clr);  // Sets 'value' to 255

        fill(value);

        int index = PApplet.parseInt(map(value, 0, 255, 14, 1));


        if (index == 1) {
          pushMatrix();
          translate(x, y);
          image(img[1], 0, 0);
          popMatrix();
        }

        if (index == 2) {
          pushMatrix();
          translate(0, 0);
          image(img[2], x, y);
          popMatrix();
        }

        if (index == 3) {
          pushMatrix();
          translate(0, 0);
          image(img[3], x, y);
          popMatrix();
        }

        if (index == 4) {
          pushMatrix();
          translate(0, 0);
          image(img[4], x, y);
          popMatrix();
        }

        if (index == 5) {
          pushMatrix();
          translate(0, 0);
          image(img[5], x, y);
          popMatrix();
        }
        if (index == 6) {
          pushMatrix();
          image(img[6], x, y);

          popMatrix();
        }
        if (index == 7) {
          pushMatrix();
          translate(0, 0);
          image(img[7], x, y);
          popMatrix();
        }
        if (index == 8) {
          pushMatrix();
          translate(0, 0);
          image(img[8], x, y);
          popMatrix();
        }
        if (index == 9) {
          pushMatrix();
          translate(0, 0);
          image(img[9], x, y);
          popMatrix();
        }
        if (index == 10) {
          pushMatrix();
          translate(0, 0);
          image(img[10], x, y);
          popMatrix();
        }
        if (index == 11) {
          pushMatrix();
          translate(0, 0);
          image(img[11], x, y);
          popMatrix();
        }
        if (index == 12) {
          pushMatrix();
          translate(0, 0);
          image(img[12], x, y);
          popMatrix();
        }
        if (index == 13) {
          pushMatrix();
          translate(0, 0);
          image(img[13], x, y);
          popMatrix();
        }
      }
  }
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "MultiScene_RECODE_00" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
